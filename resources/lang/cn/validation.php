<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines contain the default error messages used by
    | the validator class. Some of these rules have multiple versions such
    | as the size rules. Feel free to tweak each of these messages here.
    |
    */

    'accepted' => '那个 :attribute 必须被接受.',
    'active_url' => '那个 :attribute 不是有效的网址.',
    'after' => '那个 :attribute 必须是之后的日期 :date.',
    'after_or_equal' => '那个 :attribute 必须是之后的日期或等于的日期 :date.',
    'alpha' => '那个 :attribute 只能包含字母.',
    'alpha_dash' => '那个 :attribute 只能包含字母，数字，破折号和下划线.',
    'alpha_num' => '那个 :attribute 只能包含字母和数字.',
    'array' => '那个 :attribute 必须是一个数组.',
    'before' => '那个 :attribute 必须是之前的日期 :date.',
    'before_or_equal' => '那个 :attribute 必须是之前的日期或等于的日期 :date.',
    'between' => [
        'numeric' => '那个 :attribute 必须之间 :min 和 :max.',
        'file' => '那个 :attribute 必须之间 :min 和 :max 千字节.',
        'string' => '那个 :attribute 必须之间 :min 和 :max 字符.',
        'array' => '那个 :attribute 必须之间 :min 和 :max 品目.',
    ],
    'boolean' => '那个 :attribute 字段必须为真或假.',
    'confirmed' => '那个 :attribute 确认不正确.',
    'date' => '那个 :attribute 不是有效日期.',
    'date_equals' => '那个 :attribute 日期必须等于 :date.',
    'date_format' => '那个 :attribute 与格式不符 :format.',
    'different' => '那个 :attribute 和 :other 必须不同.',
    'digits' => '那个 :attribute 一定是 :digits 数字.',
    'digits_between' => '那个 :attribute 必须之间 :min 和 :max 数字.',
    'dimensions' => '那个 :attribute 图片尺寸无效.',
    'distinct' => '那个 :attribute 字段具有重复值.',
    'email' => '那个 :attribute 必须是一个有效的E-mail地址',
    'ends_with' => '那个 :attribute 必须以以下任一结尾 following: :values.',
    'exists' => '那个 selected :attribute 是无效的.',
    'file' => '那个 :attribute 必须是一个文件.',
    'filled' => '那个 :attribute 字段必须有一个值.',
    'gt' => [
        'numeric' => '那个 :attribute 必须大于 :value.',
        'file' => '那个 :attribute 必须大于 :value 千字节.',
        'string' => '那个 :attribute 必须大于 :value 字符.',
        'array' => '那个 :attribute 必须超过 :value 品目.',
    ],
    'gte' => [
        'numeric' => '那个 :attribute 必须大于或等于 :value.',
        'file' => '那个 :attribute 必须大于或等于 :value 千字节.',
        'string' => '那个 :attribute 必须大于或等于 :value 字符.',
        'array' => '那个 :attribute 一定有 :value 项以上.',
    ],
    'image' => '那个 :attribute 必须是图像.',
    'in' => '那个 selected :attribute 是无效的.',
    'in_array' => '那个 :attribute 字段不存在 :other.',
    'integer' => '那个 :attribute 必须是整数.',
    'ip' => '那个 :attribute 必须是有效的 IP 地址.',
    'ipv4' => '那个 :attribute 必须是有效的 IPv4 地址.',
    'ipv6' => '那个 :attribute 必须是有效的 IPv6 地址.',
    'json' => '那个 :attribute 必须是有效的 JSON 弦.',
    'lt' => [
        'numeric' => '那个 :attribute 必须小于 :value.',
        'file' => '那个 :attribute 必须小于 :value 千字节.',
        'string' => '那个 :attribute 必须小于 :value 字符.',
        'array' => '那个 :attribute 必须少于 :value 品目.',
    ],
    'lte' => [
        'numeric' => '那个 :attribute 必须小于或等于 :value.',
        'file' => '那个 :attribute 必须小于或等于 :value 千字节.',
        'string' => '那个 :attribute 必须小于或等于 :value 字符.',
        'array' => '那个 :attribute 可能不超过 :value 品目.',
    ],
    'max' => [
        'numeric' => '那个 :attribute 不得大于 :max.',
        'file' => '那个 :attribute 多于 :max 千字节.',
        'string' => '那个 :attribute 可能不是灰色不比 :max 字符.',
        'array' => '那个 :attribute 可能不超过 :max 品目.',
    ],
    'mimes' => '那个 :attribute 必须是以下文件 type: :values.',
    'mimetypes' => '那个 :attribute 必须是以下文件 type: :values.',
    'min' => [
        'numeric' => '那个 :attribute 必须至少 :min.',
        'file' => '那个 :attribute 必须至少 :min 千字节.',
        'string' => '那个 :attribute 必须至少 :min 字符.',
        'array' => '那个 :attribute 必须至少有 :min 品目.',
    ],
    'not_in' => '那个 selected :attribute 是无效的.',
    'not_regex' => '那个 :attribute 格式无效.',
    'numeric' => '那个 :attribute 必须是一个数字.',
    'password' => '那个 密码错误.',
    'present' => '那个 :attribute 字段必须存在.',
    'regex' => '那个 :attribute 格式无效.',
    'required' => '那个 :attribute 何时需要该字段.',
    'required_if' => '那个 :attribute 何时需要该字段 :other 是 :value.',
    'required_unless' => '那个 :attribute 必填字段，除非 :other 在 :values.',
    'required_with' => '那个 :attribute 何时需要该字段 :values 存在.',
    'required_with_all' => 'T那个he :attribute 何时需要该字段 :values 存在.',
    'required_without' => '那个 :attribute 何时需要该字段 :values 不存在.',
    'required_without_all' => '那个 :attribute 如果没有 :values 存在.',
    'same' => '那个 :attribute 和 :other 必须匹配.',
    'size' => [
        'numeric' => '那个 :attribute 必须 :size.',
        'file' => '那个 :attribute 必须 :size 千字节.',
        'string' => '那个 :attribute 必须 :size 字符.',
        'array' => '那个 :attribute 必须包含 :size 品目.',
    ],
    'starts_with' => '那个 :attribute 必须以下列之一开始 following: :values.',
    'string' => '那个 :attribute 必须是一个弦.',
    'timezone' => '那个 :attribute 必须是有效区域.',
    'unique' => '那个 :attribute 已经被使用.',
    'uploaded' => '那个 :attribute 上传失败.',
    'url' => '那个 :attribute 格式无效.',
    'uuid' => '那个 :attribute 必须是有效的 UUID.',

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | Here you may specify custom validation messages for attributes using the
    | convention "attribute.rule" to name the lines. This makes it quick to
    | specify a specific custom language line for a given attribute rule.
    |
    */

    'custom' => [
        'attribute-name' => [
            'rule-name' => '自定义消息',
        ],
    ],

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Attributes
    |--------------------------------------------------------------------------
    |
    | The following language lines are used to swap our attribute placeholder
    | with something more reader friendly such as "E-Mail Address" instead
    | of "email". This simply helps us make our message more expressive.
    |
    */

    'attributes' => [],

];
