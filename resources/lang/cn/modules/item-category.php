<?php

return [

    // Messages
    'msg_success_item_category_creation' => '产品分类已成功创建',
    'msg_success_item_category_updated' => '产品分类已成功更新',
    'msg_success_item_category_retrieved' => '产品分类已成功获得',
    'msg_success_item_category_not_found' => '找不到产品分类',
];
