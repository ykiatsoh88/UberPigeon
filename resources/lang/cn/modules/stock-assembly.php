<?php

return [

    // Return Messages
    'msg_success_listing_not_found'     => '商品组装目前没数据',
    'msg_success_listing_retrieved'     => '商品组装列表提取成功',
    'msg_success_not_found'             => '商品组装找不到',
    'msg_success_retrieved'             => '商品组装成功提取',
    'msg_success_created'               => '商品组装成功创建',
    'msg_success_updated'               => '商品组装成功更改',
    'msg_success_deleted'               => '商品组装成功删除',
    'msg_success_get_boms'              => '成功得到物料清单',
    
];
