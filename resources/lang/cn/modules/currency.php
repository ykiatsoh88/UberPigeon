<?php

return [

    //headings
    'title_currency_listing' => '货币清单',
    'title_currency_create' => '货币添加',
    'title_currency_edit' => '货币编辑',

    //placeholders
    'ph_name' => '请输入姓名',
    'ph_country' => '请选择国家',
    'ph_symbol' => '请输入符号',
    'ph_code' => '请输入验证码',

    // labels
    'lbl_country' => '国家',
    'lbl_symbol' => '符号',
    'lbl_code' => '码',
    'lbl_enable_currency' => '启用货币',
    'lbl_currency_id' => '货币 ID',
    'lbl_currency_details' => '货币明细',
    'lbl_currency_settings' => '货币设定',
    'lbl_select_one_country' => '选择一个国家',

    // Messages
    'msg_success_currency_creation' => '货币已成功创建',
    'msg_success_currency_edited' => '货币已成功更新',
    'msg_success_currency_not_found' => '找不到货币',
    'msg_success_currency_retrieved' => '货币已成功检索',

    // 'button'
    'button_create_new_currency' => '创建新货币',
    'button_add_new_currency' => '添加新货币',
];
