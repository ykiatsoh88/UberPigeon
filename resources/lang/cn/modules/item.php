<?php

return [

    // Return Messages
    'msg_success_item_creation' => '物品已成功创建',
    'msg_success_item_edited' => '物品已成功更新',
    'msg_success_item_retrieved' => '物品已成功获得',
    'msg_success_item_not_found' => '找不到物品',
];
