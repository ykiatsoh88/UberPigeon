<?php

return [

    // Return Messages
    'msg_success_item_not_found' => '找不到数据',
    'msg_success_item_retrieved' => '数据成功得到',
    'msg_success_convert_uom_not_found' => '找不到单位转换数据',
    'msg_success_convert_uom_retrieved' => '单位转换成功得到',
    'msg_success_created_convert_uom' => '单位转换成功创建',
    'msg_success_updated_convert_uom' => '单位转换成功更改',
    'msg_success_deleted_convert_uom' => '单位转换成功删除',
    
];
