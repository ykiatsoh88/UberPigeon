<?php

return [

    //headings
    'title_currency_rate_listing' => '货币汇率表',
    'title_currency_rate_create' => '货币汇率添加',
    'title_currency_rate_edit' => '货币汇率编辑',

    //placeholders
    'ph_exchange_rate' => '请输入汇率',
    'ph_inverse_rate' => '请输入反码率',
    'ph_symbol' => '请输入符号',
    'ph_code' => '请输入验证码',

    // labels
    'lbl_base_code' => '基本代码',
    'lbl_target_code' => '目标代码',
    'lbl_currency_rate_id' => '货币汇率 ID',
    'lbl_currency_rate_exchange' => '汇率',
    'lbl_currency_rate_inverse' => '逆速率',
    'lbl_modified_exchange_rate_at' => '更新汇率为',
    'lbl_modified_inverse_rate_at' => '更新的反向速率为',
    'currency_rate_details' => '货币汇率明细',
    'setting' => '设定值',

    // Messages
    'msg_success_currency_rate_creation' => '货币汇率已成功创建',
    'msg_success_currency_rate_edited' => '货币汇率已成功更新',
    'msg_success_currency_rate_retrieved' => '货币汇率已成功获得',
    'msg_success_currency_rate_not_found' => '找不到货币汇率',

    // 'button'
    'button_create_new_currency_rate' => '创建新的货币汇率',
    'button_add_new_currency_rate' => '添加新的货币汇率',
];
