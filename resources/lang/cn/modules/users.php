<?php

return [

    //headings
    'title_users_listing' => '用户列表',
    'title_users_create' => '用户添加',
    'title_users_edit' => '用户编辑',

    //placeholders
    'ph_name' => '请输入姓名',
    'ph_username' => '请输入用户名',
    'ph_email' => '请输入电子邮件',
    'ph_password' => '请输入密码',
    'ph_cpassword' => '请再次输入密码',
    'ph_old_password' => '请输入旧密码',
    'ph_new_password' => '请输入新密码',
    'ph_new_cpassword' => '请再次输入新密码',
    'ph_country' => '请选择一个国家',
    'ph_last_login_ip' => '上次登录IP',

    //txt
    'text_leave_pass_blank' => '**留空以防止密码覆盖',
    'text_my_roles' => '我分配的角色',
    'text_filtering' => '筛选',
    'text_no_roles_assigned' => '尚未分配角色',
    'text_online' => '上线',
    'text_offline' => '下线',
    'text_click_to_assign_roles_to_this_user' => '单击此处为用户分配角色',

    // labels
    'user_id' => '用户身份',
    'my_assigned_roles' => '我分配的角色',
    'last_login_ip' => '上次登录IP',
    'online_status' => '上线状态',
    'enable_user' => '启用用户',
    'user_details' => '使用者详细资料',
    'user_settings' => '用户设置',

    // Messages
    'msg_success_user_creation' => '用户已成功创建',
    'msg_success_user_edited' => '用户已成功更新',

    'msg_success_user_pass_updated' => '用户密码已成功保存',
    'msg_error_user_old_pass_invalid' => '旧密码不匹配，请重试',
    // 'button'
    'button_add_new_user' => '添加新用户',
    'button_create_new_user' => '建立新使用者',
];
