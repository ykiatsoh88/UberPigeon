<?php

return [
    'welcome_msg'                   => '欢迎来到我们的库存项目',
    'reset_msg'                     => '如果您忘记了密码或希望重设密码，请使用下方的链接重设密码',
    'order_successful_msg'          => '您的下订单成功',
];