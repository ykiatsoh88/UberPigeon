<?php

return [

    // Return Messages
    'msg_success_item_not_found' => 'Item not found',
    'msg_success_item_retrieved' => 'Item successfully retrieved',
    'msg_success_convert_uom_not_found' => 'Convert Uom not found',
    'msg_success_convert_uom_retrieved' => 'Convert Uom successfully retrieved',
    'msg_success_created_convert_uom' => 'Convert Uom successfully created',
    'msg_success_updated_convert_uom' => 'Convert Uom successfully updated',
    'msg_success_deleted_convert_uom' => 'Convert Uom successfully deleted',
    
];
