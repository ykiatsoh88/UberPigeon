<?php

return [

    // Return Messages
    'msg_success_item_creation' => 'Item has been successfully created',
    'msg_success_item_edited' => 'Item has been successfully updated',
    'msg_success_item_retrieved' => 'Item successfully retrieved',
    'msg_success_item_not_found' => 'Item not found',
];
