<?php

return [

    // Return Messages
    'msg_success_listing_not_found'     => 'No Data Found',
    'msg_success_listing_retrieved'     => 'Stock Assembly Listing Successfully Retrieved',
    'msg_success_not_found'             => 'Stock Assembly Not Found',
    'msg_success_retrieved'             => 'Stock Assembly Successfully Retrieved',
    'msg_success_created'               => 'Stock Assembly Successfully Created',
    'msg_success_updated'               => 'Stock Assembly Successfully Updated',
    'msg_success_deleted'               => 'Stock Assembly Successfully Deleted',
    'msg_success_get_boms'              => 'Successfully get stock assembly boms',
    
];
