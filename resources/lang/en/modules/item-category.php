<?php

return [

    // Messages
    'msg_success_item_category_creation' => 'Item Category has been successfully created',
    'msg_success_item_category_updated' => 'Item Category has been successfully updated',
    'msg_success_item_category_retrieved' => 'Item Category has been successfully retrieved',
    'msg_success_item_category_not_found' => 'Item Category is not found',
];
