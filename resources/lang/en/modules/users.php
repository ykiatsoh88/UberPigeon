<?php

return [

    //headings
    'title_users_listing' => 'Users Listing',
    'title_users_create' => 'User Add',
    'title_users_edit' => 'User Edit',

    //placeholders
    'ph_name' => 'Please input Name',
    'ph_username' => 'Please input Username',
    'ph_email' => 'Please input Email',
    'ph_password' => 'Please input Password',
    'ph_cpassword' => 'Please input Password again',
    'ph_old_password' => 'Please input Old Password',
    'ph_new_password' => 'Please input New Password',
    'ph_new_cpassword' => 'Please input New Password again',
    'ph_country' => 'Please select a country',
    'ph_last_login_ip' => 'Last Login Ip',

    //txt
    'text_leave_pass_blank' => '**Leave blank to prevent password overriding',
    'text_my_roles' => 'My Assigned roles',
    'text_filtering' => 'Filtering',
    'text_no_roles_assigned' => 'No assigned roles yet',
    'text_online' => 'Online',
    'text_offline' => 'Offline',
    'text_click_to_assign_roles_to_this_user' => 'Click here to assign roles to user',

    // labels
    'user_id' => 'User ID',
    'my_assigned_roles' => 'My Assigned Roles',
    'last_login_ip' => 'Last Login Ip',
    'online_status' => 'Online Status',
    'enable_user' => 'Enable User',
    'user_details' => 'User Details',
    'user_settings' => 'User Settings',

    // Messages
    'msg_success_user_creation' => 'User has been successfully created',
    'msg_success_user_edited' => 'User has been successfully updated',

    'msg_success_user_pass_updated' => 'User password have been successfully saved',
    'msg_error_user_old_pass_invalid' => 'Old Password does not match , please try again',
    // 'button'
    'button_add_new_user' => 'Add New User',
    'button_create_new_user' => 'Create new user',
];
