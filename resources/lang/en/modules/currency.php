<?php

return [

    //headings
    'title_currency_listing' => 'Currency Listing',
    'title_currency_create' => 'Currency Add',
    'title_currency_edit' => 'Currency Edit',

    //placeholders
    'ph_name' => 'Please input Name',
    'ph_country' => 'Please select Country',
    'ph_symbol' => 'Please input Symbol',
    'ph_code' => 'Please input Code',

    // labels
    'lbl_country' => 'Country',
    'lbl_symbol' => 'Symbol',
    'lbl_code' => 'Code',
    'lbl_enable_currency' => 'Enable Currency',
    'lbl_currency_id' => 'Currency Id',
    'lbl_currency_details' => 'Currency Details',
    'lbl_currency_settings' => 'Currency Settings',
    'lbl_select_one_country' => 'Select a Country',

    // Messages
    'msg_success_currency_creation' => 'Currency has been successfully created',
    'msg_success_currency_edited' => 'Currency has been successfully updated',
    'msg_success_currency_not_found' => 'Currency is not found',
    'msg_success_currency_retrieved' => 'Currency has been successfully retrieved',

    // 'button'
    'button_create_new_currency' => 'Create new Currency',
    'button_add_new_currency' => 'Add New Currency',
];
