<?php

return [

    //headings
    'title_currency_rate_listing' => 'Currency Rate Listing',
    'title_currency_rate_create' => 'Currency Rate Add',
    'title_currency_rate_edit' => 'Currency Rate Edit',

    //placeholders
    'ph_exchange_rate' => 'Please input Exchange Rate',
    'ph_inverse_rate' => 'Please input Inverse Rate',
    'ph_symbol' => 'Please input Symbol',
    'ph_code' => 'Please input Code',

    // labels
    'lbl_base_code' => 'Base Code',
    'lbl_target_code' => 'Target Code',
    'lbl_currency_rate_id' => 'Currency Rate ID',
    'lbl_currency_rate_exchange' => 'Exchange Rate',
    'lbl_currency_rate_inverse' => 'Inverse Rate',
    'lbl_modified_exchange_rate_at' => 'Updated Exchange Rate At',
    'lbl_modified_inverse_rate_at' => 'Updated Inverse Rate At',
    'currency_rate_details' => 'Currency Rate Details',
    'setting' => 'Settings',

    // Messages
    'msg_success_currency_rate_creation' => 'Currency Rate has been successfully created',
    'msg_success_currency_rate_edited' => 'Currency Rate has been successfully updated',
    'msg_success_currency_rate_retrieved' => 'Currency Rate successfully retrieved',
    'msg_success_currency_rate_not_found' => 'Currency Rate not found',

    // 'button'
    'button_create_new_currency_rate' => 'Create new Currency Rate',
    'button_add_new_currency_rate' => 'Add New Currency Rate',
];
