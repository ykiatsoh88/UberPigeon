<?php

return [
    'welcome_msg'                   => 'Welcome to Project Inventory',
    'reset_msg'                     => 'If you\'ve lost your password or wish to reset it, use the link below to get started.',
    'order_successful_msg'          => 'Order placed successful',
];