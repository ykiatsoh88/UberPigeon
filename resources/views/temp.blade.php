<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>Laravel File Upload</title>
    <script src="https://cdn.jsdelivr.net/npm/jquery@3.5.1/dist/jquery.min.js"></script>
    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.2/dist/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.2/dist/css/bootstrap.css">
    <!-- Styles -->
    <style>
        html,
        body {
            background-color: #fff;
            color: #636b6f;
            font-family: 'Nunito', sans-serif;
            font-weight: 200;
            height: 100vh;
            margin: 0;
        }

        .full-height {
            height: 100vh;
        }

        .flex-center {
            align-items: center;
            display: flex;
            justify-content: center;
        }

        .position-ref {
            position: relative;
        }

        .top-right {
            position: absolute;
            right: 10px;
            top: 18px;
        }

        .content {
            text-align: center;
        }

        .title {
            font-size: 84px;
        }

        .links>a {
            color: #636b6f;
            padding: 0 25px;
            font-size: 13px;
            font-weight: 600;
            letter-spacing: .1rem;
            text-decoration: none;
            text-transform: uppercase;
        }

        .m-b-md {
            margin-bottom: 30px;
        }
    </style>
</head>

<body>
    <div class="flex-center position-ref full-height">
        <div class="content">
            <div class="title m-b-md">
                Laravel File Upload
            </div>



            <div class="container">
                <h1>Jquery Upload</h1>
                <form method="post" action="" enctype="multipart/form-data" id="myform">
                    <div id="loading"></div>
                    <div class='preview'>
                        <img src="https://via.placeholder.com/150/" id="img" >
                    </div>
                    <div>
                        <input type="file" id="file" name="file" />
                        <input type="button" class="button" value="Upload" id="but_upload">
                    </div>
                </form>
            </div>
            <hr />

            @isset($success)
            <div class="alert alert-success alert-block">
                <button type="button" class="close" data-dismiss="alert">×</button>
                <strong>{{ $success }}</strong>
            </div>

            <div>
                <img src="{{$img_url ?? ''}}" alt="image">
            </div>
            @endisset


            <div class="links">
                <h1>Postback Upload</h1>
                <form action="/upload" method="POST" enctype="multipart/form-data">
                    @csrf
                    <div class="row">

                        <div class="col-md-6">
                            <input type="file" name="file" class="form-control">
                        </div>

                        <div class="col-md-6">
                            <button type="submit" class="btn btn-success">Upload a File</button>
                        </div>

                    </div>
                </form>
            </div>
        </div>
    </div>
    <script>
        $(document).ready(function(){
            $.ajaxSetup({
            headers : {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $("#but_upload").click(function(){

            var fd = new FormData();
            var files = $('#file')[0].files[0];
            let upload_url = '{{route('upload')}}';
            fd.append('file',files);

            $.ajax({
                url: upload_url,
                type: 'post',
                data: fd,
                contentType: false,
                processData: false,
                beforeSend: function() {
                $('#loading').html('Uplading . . .');    
                },
                success: function(response){
                    if(response != 0){
                        $('#loading').empty();    
                        $("#img").attr("src",response); 
                        $(".preview img").show(); // Display image element
                    }else{
                        alert('file not uploaded');
                    }
                },
            });
        });

        });

    </script>
</body>

</html>