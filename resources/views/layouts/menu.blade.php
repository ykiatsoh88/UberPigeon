<!-- need to remove -->
<li class="nav-item">
    <a href="{{ route('home') }}" class="nav-link {{ (request()->routeIs('home')) ? 'active' : '' }}">
        <i class="nav-icon fas fa-home"></i>
        <p>@lang('menu.home')</p>
    </a>
</li>

<li class="nav-item">
    <a href="{{ route('users.index') }}" class="nav-link {{ (request()->routeIs('users.index')) ? 'active' : '' }} ">
        <i class="nav-icon fas fa-user"></i>
        <p>@lang('menu.users')</p>
    </a>
</li>

<li class="nav-item">
    <a href="{{ route('platform-wallet.index') }}" class="nav-link {{ (request()->routeIs('platform-wallet.index')) ? 'active' : '' }} ">
        <i class="nav-icon fas fa-wallet"></i>
        <p>@lang('menu.platform_wallet')</p>
    </a>
</li>

<li class="nav-item">
    <a href="{{ route('members.index') }}" class="nav-link {{ (request()->routeIs('members.index')) ? 'active' : '' }} ">
        <i class="nav-icon fas fa-users"></i>
        <p>@lang('menu.members')</p>
    </a>
</li>

<li class="nav-item">
    <a href="{{ route('merchants.index') }}" class="nav-link {{ (request()->routeIs('merchants.index')) ? 'active' : '' }} ">
        <i class="nav-icon fas fa-store"></i>
        <p>@lang('menu.merchants')</p>
    </a>
</li>

<li class="nav-item">
    <a href="{{ route('products.index') }}" class="nav-link {{ (request()->routeIs('products.index')) ? 'active' : '' }} ">
        <i class="nav-icon fas fa-boxes"></i>
        <p>@lang('menu.products')</p>
    </a>
</li>

<li class="nav-item">
    <a href="{{ route('sales.order.index') }}" class="nav-link {{ (request()->routeIs('sales.order.index')) ? 'active' : '' }} ">
        <i class="nav-icon fas fa-cart-arrow-down"></i>
        <p>@lang('menu.sales_orders')</p>
    </a>
</li>

<li class="nav-item">
    <a href="{{ route('slider.index') }}" class="nav-link {{ (request()->routeIs('slider.index')) ? 'active' : '' }} ">
        <i class="nav-icon fas fa-object-group"></i>
        <p>@lang('menu.sliders')</p>
    </a>
</li>

<li class="nav-item has-treeview">
    <a href="#" class="nav-link">
        <i class="nav-icon fas fa-money-bill-alt"></i>
        <p>@lang('menu.coin_withdrawal')</p>
        <i class="right fas fa-angle-left"></i> 
    </a>

    <ul class="nav nav-treeview" data-widget="treeview">
        <li class="nav-item">
            <a href="{{ route('merchant-withdrawal-approval.index') }}" class="nav-link {{ (request()->routeIs('merchant-withdrawal-approval.index')) ? 'active' : '' }} ">
                <i class="far fa-circle nav-icon"></i>
                <p>@lang('menu.merchant_withdrawal')</p>
            </a>
        </li>

        <li class="nav-item">
            <a href="{{ route('member-withdrawal-approval.index') }}" class="nav-link {{ (request()->routeIs('member-withdrawal-approval.index')) ? 'active' : '' }} ">
                <i class="far fa-circle nav-icon"></i>
                <p>@lang('menu.member_withdrawal')</p>
            </a>
        </li>
    </ul>
</li>

<li class="nav-item has-treeview">
    <a href="#" class="nav-link">
        <i class="nav-icon fas fa-industry"></i>
        <p>@lang('menu.inventories')</p>
        <i class="right fas fa-angle-left"></i> 
    </a>

    <ul class="nav nav-treeview" data-widget="treeview">
        <li class="nav-item">
            <a href="{{ route('inventory-summary.index') }}" class="nav-link {{ (request()->routeIs('inventory-summary.index')) ? 'active' : '' }} ">
                <i class="far fa-circle nav-icon"></i>
                <p>@lang('menu.inventory_summary')</p>
            </a>
        </li>

        <li class="nav-item">
            <a href="{{ route('inventory-adjustment.index') }}" class="nav-link {{ (request()->routeIs('inventory-adjustment.index')) ? 'active' : '' }} ">
                <i class="far fa-circle nav-icon"></i>
                <p>@lang('menu.inventory_adjustment')</p>
            </a>
        </li>

        <li class="nav-item">
            <a href="{{ route('inventory-received.index') }}" class="nav-link {{ (request()->routeIs('inventory-received.index')) ? 'active' : '' }} ">
                <i class="far fa-circle nav-icon"></i>
                <p>@lang('menu.inventory_received')</p>
            </a>
        </li>
    </ul>
</li>

<li class="nav-item has-treeview">
    <a href="#" class="nav-link">
        <i class="nav-icon fas fa-book"></i>
        <p>@lang('menu.masters')</p>
        <i class="right fas fa-angle-left"></i> 
    </a>

    <ul class="nav nav-treeview" data-widget="treeview">
        <li class="nav-item">
            <a href="{{ route('attributes.index') }}" class="nav-link {{ (request()->routeIs('attributes.index')) ? 'active' : '' }} ">
                <i class="far fa-circle nav-icon"></i>
                <p>@lang('menu.attributes')</p>
            </a>
        </li>

        <li class="nav-item">
            <a href="{{ route('attribute-options.index') }}" class="nav-link {{ (request()->routeIs('attribute-options.index')) ? 'active' : '' }} ">
                <i class="far fa-circle nav-icon"></i>
                <p>@lang('menu.attribute_options')</p>
            </a>
        </li>

        <li class="nav-item">
            <a href="{{ route('category.index') }}" class="nav-link {{ (request()->routeIs('category.index')) ? 'active' : '' }} ">
                <i class="far fa-circle nav-icon"></i>
                <p>@lang('menu.categories')</p>
            </a>
        </li>

        <li class="nav-item">
            <a href="{{ route('cities.index') }}" class="nav-link {{ (request()->routeIs('cities.index')) ? 'active' : '' }} ">
                <i class="far fa-circle nav-icon"></i>
                <p>@lang('menu.cities')</p>
            </a>
        </li>

        <li class="nav-item">
            <a href="{{ route('coins.index') }}" class="nav-link {{ (request()->routeIs('coins.index')) ? 'active' : '' }} ">
                <i class="far fa-circle nav-icon"></i>
                <p>@lang('menu.coins')</p>
            </a>
        </li>

        <li class="nav-item">
            <a href="{{ route('countries.index') }}" class="nav-link {{ (request()->routeIs('countries.index')) ? 'active' : '' }} ">
                <i class="far fa-circle nav-icon"></i>
                <p>@lang('menu.countries')</p>
            </a>
        </li>
                
        <li class="nav-item">
            <a href="{{ route('currency.index') }}" class="nav-link {{ (request()->routeIs('currency.index')) ? 'active' : '' }} ">
                <i class="far fa-circle nav-icon"></i>
                <p>@lang('menu.currencies')</p>
            </a>
        </li>

        <li class="nav-item">
            <a href="{{ route('currency_rate.index') }}" class="nav-link {{ (request()->routeIs('currency_rate.index')) ? 'active' : '' }} ">
                <i class="far fa-circle nav-icon"></i>
                <p>@lang('menu.currency_rates')</p>
            </a>
        </li>

        <li class="nav-item">
            <a href="{{ route('delivery-company.index') }}" class="nav-link {{ (request()->routeIs('delivery-company.index')) ? 'active' : '' }} ">
                <i class="far fa-circle nav-icon"></i>
                <p>@lang('menu.delivery_companies')</p>
            </a>
        </li>
                
        <li class="nav-item">
            <a href="{{ route('sales_status.index') }}" class="nav-link {{ (request()->routeIs('sales_status.index')) ? 'active' : '' }} ">
                <i class="far fa-circle nav-icon"></i>
                <p>@lang('menu.sales_statuses')</p>
            </a>
        </li>

        <li class="nav-item">
            <a href="{{ route('shipping.index') }}" class="nav-link {{ (request()->routeIs('shipping.index')) ? 'active' : '' }} ">
                <i class="far fa-circle nav-icon"></i>
                <p>@lang('menu.shipping')</p>
            </a>
        </li>

        <li class="nav-item">
            <a href="{{ route('states.index') }}" class="nav-link {{ (request()->routeIs('states.index')) ? 'active' : '' }} ">
                <i class="far fa-circle nav-icon"></i>
                <p>@lang('menu.states')</p>
            </a>
        </li>
    </ul>
</li>

@push('page_scripts')
    <script type="text/javascript">

        $(document).ready(function() {
            /** add active class and stay opened when selected */
            var url = window.location;

            // for sidebar menu entirely but not cover treeview
            $('ul.nav-sidebar a').filter(function() {
                return this.href == url;
            }).addClass('active');

            // for treeview
            $('ul.nav-treeview a').filter(function() {
                return this.href == url;
            }).parentsUntil(".nav-sidebar > .nav-treeview").addClass('menu-open').prev('a').addClass('active');
        })

    </script>
@endpush

