@component('mail::message')
# Order Successfully Paid


Hi {{ $name }} , {{ $msg }}



@component('mail::button', ['url' => $view_orders_url])
Click Here to ciew your order(s)
@endcomponent

Thanks,<br>
{{ config('app.name') }}
@endcomponent
