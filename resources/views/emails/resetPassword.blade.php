@component('mail::message')
# Reset Password


Hi {{ $name }} , {{ $msg }}



@component('mail::button', ['url' => $reset_password_url])
Click Here to reset your password
@endcomponent

Thanks,<br>
{{ config('app.name') }}
@endcomponent
