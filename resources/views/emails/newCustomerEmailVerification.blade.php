@component('mail::message')
# Email Verification

Hi {{ $name }} , {{ $msg }} 

@component('mail::button', ['url' => $verification_url])
Click Here to verify your email
@endcomponent

Thanks,<br>
{{ config('app.name') }}
@endcomponent
