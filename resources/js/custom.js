function convertFormToJSON(form, exclude_fields) {
    exclude_fields = exclude_fields || [];
    var array = jQuery(form).serializeArray();
    var json = {};

    //console.log(exclude_fields);
    jQuery.each(array, function() {
        //console.log(this.name);
        if (exclude_fields.length > 0) {
            if (jQuery.inArray(this.name, exclude_fields) >= 0) {
                //console.log('xclude' + this.name  + 'result' + jQuery.inArray(this.name , exclude_fields));
                return true;
            }
        }

        if (this.name in json) {
            var tmp = "";

            if (jQuery.type(json[this.name]) == "string") {
                json[this.name] += "," + this.value;
                tmp = json[this.name].split(",");
                json[this.name] = tmp;
            } else if (jQuery.type(json[this.name]) == "array") {
                json[this.name].push(this.value);
            }
        } else {
            json[this.name] = this.value || "";
        }
    });

    return json;
}
