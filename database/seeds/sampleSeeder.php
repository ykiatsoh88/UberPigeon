<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class sampleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        DB::table('pigeon')->delete();
        DB::table('pigeon')->insert([
            array('id' => '1','name' => 'Antonio','speed' => '70','range' => '600','cost' => '2.00','downtime' => '2.00','status' => '1','created_at' => '2021-03-14 21:26:43','updated_at' => NULL,'deleted_at' => NULL),
            array('id' => '2','name' => 'Bonito','speed' => '80','range' => '500','cost' => '2.00','downtime' => '3.00','status' => '1','created_at' => '2021-03-08 21:27:53','updated_at' => NULL,'deleted_at' => NULL),
            array('id' => '3','name' => 'Carillo','speed' => '65','range' => '1000','cost' => '2.00','downtime' => '3.00','status' => '1','created_at' => '2021-03-04 21:27:56','updated_at' => NULL,'deleted_at' => NULL),
            array('id' => '4','name' => 'Alejandro','speed' => '70','range' => '800','cost' => '2.00','downtime' => '2.00','status' => '1','created_at' => '2021-03-05 21:27:58','updated_at' => NULL,'deleted_at' => NULL)
        ]);

    }
}
