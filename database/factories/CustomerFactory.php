<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model;
use App\Models\Customer;
use Faker\Generator as Faker;

$factory->define(Customer::class, function (Faker $faker) {

    $gender = Arr::random(['male', 'female']);
    $fillGender = ($gender == 'male' ? "M" : "F");
    //$companyName = Arr::random(['Test Shop', 'Test Shop 2' , 'Test Shop 3' , 'Test Shop 4']);
    $firstname = $faker->firstName($gender);
    $companyName =  $firstname . " shop";

    return [
        'first_name'            => $firstname,
        'last_name'             => $faker->lastName($gender),
        'phone_no'              => $faker->unique()->numerify('##########'),
        'email'                 => $faker->unique()->safeEmail,
        'password'              => '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', // password
        'is_email_verified'     => Arr::random([0, 1]),
        'status'                => Arr::random([0, 1]),
        'street_one'            => $faker->streetaddress,
        'street_two'            => $faker->streetaddress,
        'postcode_id'           => Arr::random([1, 50]),
        'city_id'               => Arr::random([1, 30]),
        'state_id'              => Arr::random([1, 10]),
        'country_id'            => Arr::random([1, 2]),
        'company_ssm_no'        => $faker->randomNumber(5, false),
        'company_name'          => $companyName,
        'company_phone_no'      => $faker->unique()->numerify('##########'),
        'company_street_one'    => $faker->streetaddress,
        'company_street_two'    => $faker->streetaddress,
        'company_postcode_id'   => Arr::random([1, 50]),
        'company_city_id'       => Arr::random([1, 30]),
        'company_state_id'      => Arr::random([1, 10]),
        'company_country_id'    => 1 ,//Arr::random([1, 2]),
        'is_company_verified'   => Arr::random([0, 1]),
        'shop_name'             => $companyName,
        'shop_img_url'          => 'https://res.cloudinary.com/dalzunnpc/image/upload/v1603601939/products/tay8yngv7q7fx0lpovwx.jpg',//$faker->url,
        'transaction_fee_charge'=> 0,
    ];
});
