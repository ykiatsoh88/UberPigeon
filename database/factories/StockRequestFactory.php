<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */
// https://github.com/fzaninotto/Faker#fakerproviderdatetime

use App\Models\StockRequest;
use Faker\Generator as Faker;

$factory->define(StockRequest::class, function (Faker $faker) {
    return [
        'customer_id' => $faker->randomDigit,
        'document_number' => $faker->randomNumber,
        'document_date' => $faker->dateTimeThisYear('now',null),
        'description' => $faker->text(200),
        'requestor' => 1,
        'status' => 1,
        'created_by' => 1,
        'updated_by' =>1,
        'created_at' => $faker->dateTime('now',null),
        'updated_at' => $faker->dateTime('now',null),
        'deleted_at' => null
    ];
});
