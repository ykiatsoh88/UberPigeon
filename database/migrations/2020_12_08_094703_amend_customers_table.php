<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AmendCustomersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::table('customers', function (Blueprint $table) {
            $table->string('first_name', 100)->nullable()->after('id');
            $table->string('last_name', 100)->nullable()->after('id');
            $table->string('phone_no', 25)->nullable()->after('email');
            $table->integer('parent_account')->default(0)->comment('0 is merchant account, > 0 is merchant\'s user account')->after('id');
            $table->tinyInteger('is_email_verified')->nullable()->comment('customer account verified or not')->after('email');
            $table->text('street_one')->nullable()->after('email');
            $table->text('street_two')->nullable()->after('email');
            $table->integer('postcode_id')->nullable()->after('email');
            $table->integer('city_id')->nullable()->after('email');
            $table->integer('state_id')->nullable()->after('email');
            $table->integer('country_id')->nullable()->after('email');
            $table->dropColumn('name');
            $table->dropColumn('username');
            $table->dropColumn('email_verified_at');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('customers', function (Blueprint $table) {
            $table->dropColumn('first_name');
            $table->dropColumn('last_name');
            $table->dropColumn('phone_no');
            $table->dropColumn('parent_account');
            $table->dropColumn('is_email_verified');
            $table->dropColumn('street_one');
            $table->dropColumn('street_two');
            $table->dropColumn('postcode_id');
            $table->dropColumn('city_id');
            $table->dropColumn('state_id');
            $table->dropColumn('country_id');
            $table->string('username', 150)->unique();
            $table->string('name')->after('id');
            $table->timestamp('email_verified_at')->nullable()->after('email');
        });
    }
}
