<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('customer_id');
            $table->unsignedBigInteger('pigeon_id')->nullable();
            $table->dateTime('deadline', 0);
            $table->integer('distance')->comment('delivery distance');
            $table->text('pickup_address')->comment('document pickup address');
            $table->string('pickup_coordinates', 100)->comment('document pickup latitude & longitude');
            $table->text('delivery_address')->comment('document delivery address');
            $table->string('delivery_coordinates', 100)->comment('document delivery latitude & longitude');
            $table->text('remark')->comment('order remark')->nullable();
            $table->text('reject_reason')->nullable();
            $table->text('cancel_reason')->nullable();
            $table->decimal('total')->comment('order total amount')->nullable();
            $table->tinyInteger('status')->comment('0 = pending, 1 = accepted, 2 = rejected, 3 = cancelled, 4 = received');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
