<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePigeonTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pigeon', function (Blueprint $table) {
            $table->id();
            $table->string('name',255);
            $table->integer('speed')->comment('km per hour');
            $table->integer('range')->comment('km');
            $table->decimal('cost')->comment('per km');
            $table->decimal('downtime')->comment('per hour');
            $table->tinyInteger('status')->comment('0 = disable, 1 = enable');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pigeon');
    }
}
