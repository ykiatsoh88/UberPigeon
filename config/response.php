<?php


return [

        'success' => 1, 
        'fail'   => 10 ,
        'not_exist' => 11 ,
        'disabled' => 12 , 
        'validation_error' => 20 , 
        
        'invalid_api_access' => 100 ,

        'token_invalid' => 300,
        'token_expired' => 310 ,
        
        'token_blacklisted' => 333,
        'token_not_provided' => 355,


        'otp_invalid' => 400,
        'error' => 500,
        'sql_error' => 560,

        'maintenance' => 888

];