<?php

namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Tymon\JWTAuth\Contracts\JWTSubject;
use Spatie\Permission\Traits\HasRoles;

class Customer extends Authenticatable implements JWTSubject
{
    use Notifiable;
    use SoftDeletes;
    use HasRoles;

    protected $table = 'customers';
    protected $fillable = [
        'parent_account',
        'first_name',
        'last_name',
        'email',
        'country_id',
        'street_one',
        'street_two',
        'postcode_id',
        'city_id',
        'state_id',
        'is_email_verified',
        'email_verified_at',
        'phone_no',
        'username',
        'password',
        'status',
        'last_login',
        'created_by',
        'updated_by',
        'created_at',
        'remember_token',
        'email_verify_retries',
        'updated_at'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
    ];

    protected $appends = [
        'name'
    ];

    protected $guard_name = "user-api";

    public function setPasswordAttribute($value)
    {
        $this->attributes['password'] = bcrypt($value);
    }

    public function getNameAttribute()
    {
        return $this->attributes['first_name'] . ' ' .  $this->attributes['last_name'];
    }

    public static function getCustomerById($customer_id)
    {

        $customer = Customer::find($customer_id);

        $result = $customer ?
            array('result' => 1, 'customer' => $customer->toArray(), 'message' => 'Successful') :
            array('result' => -1, 'customer' => [], 'message' => 'No record found');

        return $result;
    }

    public static function getCustomersByCountryId($country_id)
    {

        $customer = Customer::query()
            ->where('country_id', '=', $country_id)
            ->get();

        $result = $customer ?
            array('result' => 1, 'customer' => $customer->toArray(), 'message' => 'Successful') :
            array('result' => -1, 'customer' => [], 'message' => 'No record found');

        return $result;
    }

    public static function getCustomerByCompanyName($company_name)
    {

        $customer = Customer::query()
            ->where('company_name', '=', $company_name)
            ->first();

        $result = $customer ?
            array('result' => 1, 'customer' => $customer->toArray(), 'message' => 'Successful') :
            array('result' => -1, 'customer' => [], 'message' => 'No record found');

        return $result;
    }

    public static function getAllCustomerCompanyName()
    {

        $customer = Customer::query()
            ->select([
                'id',
                'company_name',
                'first_name',
                'last_name'
            ])
            ->where('status', '=', 1)
            ->get();

        $result = $customer ?
            array('result' => 1, 'customer' => $customer->toArray(), 'message' => 'Successful') :
            array('result' => -1, 'customer' => [], 'message' => 'No record found');

        return $result;
    }

    /**
     * Get the identifier that will be stored in the subject claim of the JWT.
     *
     * @return mixed
     */
    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    /**
     * Return a key value array, containing any custom claims to be added to the JWT.
     *
     * @return array
     */
    public function getJWTCustomClaims()
    {
        return [
            'grd' => 'usr'  // guard = member
        ];
    }
}
