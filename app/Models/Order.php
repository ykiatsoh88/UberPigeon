<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Order extends Model
{
    use SoftDeletes;

    protected $table = 'orders'; 

    protected $fillable = [
        'customer_id', 
        'pigeon_id', 
        'deadline', 
        'distance', 
        'pickup_address', 
        'pickup_coordinates', 
        'delivery_address', 
        'delivery_coordinates', 
        'remark', 
        'total',
        'reject_reason',
        'cancel_reason',
        'status'
    ];

}
