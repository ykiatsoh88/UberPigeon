<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Pigeon extends Model
{
    use SoftDeletes;

    protected $table = 'pigeon'; 

    protected $fillable = [
        'name', 
        'speed', 
        'range', 
        'cost', 
        'downtime', 
        'status'
    ];

}
