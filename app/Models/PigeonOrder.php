<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PigeonOrder extends Model
{
    protected $table = 'pigeon_order'; 

    protected $fillable = [
        'order_id', 
        'pigeon_id', 
        'estimate_delivery_time', 
        'actual_delivery_time', 
        'status'
    ];

}
