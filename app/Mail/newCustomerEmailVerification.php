<?php

namespace App\Mail;

use App\Lib\OTP\OTP;
use App\Models\Customer;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class newCustomerEmailVerification extends Mailable
{
    use Queueable, SerializesModels;

    public $customer;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Customer $customer)
    {
        $this->customer = $customer;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $otp = new OTP();
        $email = $this->customer->email;
        $name = $this->customer->name;
        $otp_code = $otp->generateRandomString(6);
        $hash = $otp->CreateOTP($email, $otp_code);
        $msg = trans('email/default.welcome_msg');

        $verification_url = env('USER_PORTAL_URL') . "verify-customer/$email/$hash/$otp_code";

        return $this->markdown('emails.newCustomerEmailVerification')->with(compact('msg', 'name', 'verification_url'));
    }
}
