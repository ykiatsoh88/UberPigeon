<?php

namespace App\Mail;

use App\Lib\OTP\OTP;
use App\Models\Customer;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Support\Facades\Cache;
use Illuminate\Queue\SerializesModels;

class newResetPasswordEmail extends Mailable
{
    use Queueable, SerializesModels;

    public $customer;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Customer $customer)
    {
        $this->customer = $customer;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $otp = new OTP();
        $email = $this->customer->email;
        $name = $this->customer->name;
        $otp_code = $otp->generateRandomString(6);
        $hash = $otp->CreateOTP($email, $otp_code);
        $msg = trans('email/default.reset_msg');
        
        Cache::put($otp_code, $email, now()->addMinutes(5));

        $reset_password_url = env('USER_PORTAL_URL') . "password/reset/$email/$hash/$otp_code";

        return $this->markdown('emails.resetPassword')->with(compact('msg', 'name', 'reset_password_url'));
    }
}
