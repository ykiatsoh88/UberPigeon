<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class WelcomeNewUserMail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($data)
    {
        $this->data = $data;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $email = (empty($this->data['email']))?'no-email@email.com':$this->data['email'];
        $name = (empty($this->data['name']))?'no-name':$this->data['name'];
        $msg = (empty($this->data['msg']))?'no-msg':$this->data['msg'];

        return $this->markdown('emails.new-welcome')->with(compact('msg', 'name', 'email'));
    }
}
