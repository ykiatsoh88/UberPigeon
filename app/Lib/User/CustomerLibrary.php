<?php

namespace App\Lib\User;

use Validator;
use ErrorMessage;
use App\Helpers\Common;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\DB;
use App\Models\Customer as ModelCustom;
use App\Models\ConsolidatedUser;
use Illuminate\Foundation\Validation\ValidatesRequests;
use App\Lib\AuditTrail\AuditTrailLibrary;

class CustomerLibrary
{
    use ValidatesRequests;
    protected $table = 'customers';
    protected $table_desc = 'Customer';
    private const CREATE = 'Create';
    private const UPDATE = 'Update';
    private const DELETE = 'Delete';
    private const ADMIN = 'Admin';
    private const MODULENAME = 'Customer';
    private $auditTrailLibrary;

    public function __construct()
    {
        $this->auditTrailLibrary = new AuditTrailLibrary();
    }


    public function getAll($offset = 0, $limit = 20, $customer_id = 0)
    {
        try {
            $data = [];
            $query = ModelCustom::with('roles:id,name')
                ->offset($offset)
                ->limit($limit)
                ->get();

            if ($query->isEmpty()) {
                return \API::jsonResponse(['result' => \APIResultResponse::SUCCESS, 'error' => $this->table_desc . ' is not found'], \APIHttpResponse::OK);
            }

            foreach ($query as $queryItem) {
                $roles = $this->convertListOfObjectsToArrays($queryItem['roles'], 'name');
                unset($queryItem['roles']);
                $queryItem['roles'] = $roles;
            }

            $data['data'] = $query;
            $data['count'] = count(ModelCustom::get());

            return \API::jsonResponse(['result' => \APIResultResponse::SUCCESS, 'data' => $data, 'message' => $this->table_desc . ' has been retreived successfully'], \APIHttpResponse::OK);
        } catch (\Exception $e) {
            return \API::jsonResponse(['result' => \APIResultResponse::ERROR, 'error' => $e->getMessage()], \APIHttpResponse::ERROR);
        }
    }

    public function getAllById($id, $customer_id = 0)
    {
        try {
            $query = ModelCustom::with('roles:id,name')
                ->where('id', '=', $id)
                ->first();

            if (empty($query)) {

                return \API::jsonResponse(['result' => \APIResultResponse::SUCCESS, 'error' => $this->table_desc . ' is not found'], \APIHttpResponse::OK);
            }

            $roles = $this->convertListOfObjectsToArrays($query->roles, 'name');

            unset($query['roles']);

            $query->roles = $roles;

            return \API::jsonResponse(['result' => \APIResultResponse::SUCCESS, 'data' => $query, 'message' => $this->table_desc . ' has been retreived successfully'], \APIHttpResponse::OK);
        } catch (\Exception $e) {
            return \API::jsonResponse(['result' => \APIResultResponse::ERROR, 'error' => $e->getMessage()], \APIHttpResponse::ERROR);
        }
    }

    public function create(Request $request)
    {
        try {
            $validatorCustom = $this->validateCustom($request);
            $validatorRole = $this->validateRoles($request);

            if ($validatorCustom->fails()) {
                return \API::jsonResponse(['result' => \APIResultResponse::VALIDATION_ERROR, 'error' => $validatorCustom->errors()], \APIHttpResponse::VALIDATION_ERROR);
            }

            if ($validatorRole->fails()) {
                return \API::jsonResponse(['result' => \APIResultResponse::VALIDATION_ERROR, 'error' => $validatorRole->errors()], \APIHttpResponse::VALIDATION_ERROR);
            }

            $dataRole = $validatorRole->validated();

            $query = ModelCustom::create($request->all());

            $query->save();

            $query->assignRole($dataRole);

            $consolidateUsr = ConsolidatedUser::create([
                'user_type' => 'usr',
                'user_id' => $query->id
            ]);

            $consolidateUsr->save();

            $message = $this->table_desc . ' has been created successfully';

            //seed CRN based on customerId
            $customer_id = $query->id;
            DB::table('customer_running_number')->insert([
                ['customer_id' => $customer_id, 'module_id' => 1, 'prefix' => 'SI', 'number_format' => '0000', 'running_no' => 1, 'status' => 1],
                ['customer_id' => $customer_id, 'module_id' => 2, 'prefix' => 'RQ', 'number_format' => '0000', 'running_no' => 1, 'status' => 1],
                ['customer_id' => $customer_id, 'module_id' => 3, 'prefix' => 'RC', 'number_format' => '0000', 'running_no' => 1, 'status' => 1],
                ['customer_id' => $customer_id, 'module_id' => 4, 'prefix' => 'XFER', 'number_format' => '0000', 'running_no' => 1, 'status' => 1],
                ['customer_id' => $customer_id, 'module_id' => 5, 'prefix' => 'ADJ', 'number_format' => '0000', 'running_no' => 1, 'status' => 1],
                ['customer_id' => $customer_id, 'module_id' => 6, 'prefix' => 'GRN', 'number_format' => '0000', 'running_no' => 1, 'status' => 1],
                ['customer_id' => $customer_id, 'module_id' => 7, 'prefix' => 'GR', 'number_format' => '0000', 'running_no' => 1, 'status' => 1],
                ['customer_id' => $customer_id, 'module_id' => 9, 'prefix' => 'CU', 'number_format' => '0000', 'running_no' => 1, 'status' => 1],
                ['customer_id' => $customer_id, 'module_id' => 10, 'prefix' => 'SA', 'number_format' => '0000', 'running_no' => 1, 'status' => 1],
            ]);

            $auditTrailData = [
                'event_date' => date('Y-m-d H:i:s'),
                'user_id' => (auth('admin-api')->check())? auth('admin-api')->User()->id : 0,
                'module_name' => SELF::MODULENAME,
                'event_type' => SELF::CREATE,
                'description' => $message,
                'event_message' => json_encode($query, JSON_PRETTY_PRINT)
            ];

            $this->auditTrailLibrary->insertAuditTrail($auditTrailData, SELF::ADMIN);

            return \API::jsonResponse(['result' => \APIResultResponse::SUCCESS, 'data' => $query, 'message' => $message], \APIHttpResponse::OK);
        } catch (\Exception $e) {
            return \API::jsonResponse(['result' => \APIResultResponse::ERROR, 'error' => $e->getMessage()], \APIHttpResponse::ERROR);
        }
    }

    public function update(Request $request, $id)
    {
        try {
            $validatorCustom = $this->validateCustom($request, 'update', $id);
            $validatorRole = $this->validateRoles($request);

            if ($validatorCustom->fails()) {
                return \API::jsonResponse(['result' => \APIResultResponse::VALIDATION_ERROR, 'error' => $validatorCustom->errors()], \APIHttpResponse::VALIDATION_ERROR);
            }

            if ($validatorRole->fails()) {
                return \API::jsonResponse(['result' => \APIResultResponse::VALIDATION_ERROR, 'error' => $validatorRole->errors()], \APIHttpResponse::VALIDATION_ERROR);
            }

            $dataQuery = $validatorCustom->validated();
            $dataRole = $validatorRole->validated();

            $query = ModelCustom::findOrFail($id);

            $eventMessage = 'Before' . "\n" . json_encode($query, JSON_PRETTY_PRINT) . "\n\n";

            $query->fill($request->all());

            $query->save();

            $query->syncRoles($dataRole);

            $message = $this->table_desc . ' has been updated successfully';

            $eventMessage .= 'After' . "\n" . json_encode($query, JSON_PRETTY_PRINT);

            $auditTrailData = [
                'event_date' => date('Y-m-d H:i:s'),
                'user_id' => (auth('admin-api')->check())? auth('admin-api')->User()->id : 0,
                'module_name' => SELF::MODULENAME,
                'event_type' => SELF::UPDATE,
                'description' => $message,
                'event_message' => json_encode($eventMessage)
            ];

            $this->auditTrailLibrary->insertAuditTrail($auditTrailData, SELF::ADMIN);

            return \API::jsonResponse(['result' => \APIResultResponse::SUCCESS, 'data' => $query, 'message' => $message], \APIHttpResponse::OK);
        } catch (\Exception $e) {
            return \API::jsonResponse(['result' => \APIResultResponse::ERROR, 'error' => $e->getMessage()], \APIHttpResponse::ERROR);
        }
    }

    public function delete($id)
    {
        try {
            $query = ModelCustom::findOrFail($id);

            $message = $this->table_desc . ' has been deleted successfully';

            $auditTrailData = [
                'event_date' => date('Y-m-d H:i:s'),
                'user_id' => (auth('admin-api')->check())? auth('admin-api')->User()->id : 0,
                'module_name' => SELF::MODULENAME,
                'event_type' => SELF::DELETE,
                'description' => $message,
                'event_message' => json_encode($query, JSON_PRETTY_PRINT)
            ];

            $this->auditTrailLibrary->insertAuditTrail($auditTrailData, SELF::ADMIN);

            $query->syncRoles([]);
            $query->delete();

            return \API::jsonResponse(['result' => \APIResultResponse::SUCCESS, 'message' => $message], \APIHttpResponse::OK);
        } catch (\Exception $e) {
            return \API::jsonResponse(['result' => \APIResultResponse::ERROR, 'error' => $e->getMessage()], \APIHttpResponse::ERROR);
        }
    }

    public function search(Request $request)
    {
        try {
            $data = [];
            $availableFields = DB::getSchemaBuilder()->getColumnListing($this->table);
            $query = ModelCustom::query()
                ->select($availableFields);

            foreach ($availableFields as $af_key => $af_val) {

                if ($af_val == 'status') {
                    if (Common::requestHas($request, 'status', true)) {
                        $query->where('status', '=', $request->input('status'));
                    }
                } else {
                    if (Common::requestHas($request, $af_val)) {
                        $query->where($af_val, 'like', '%' . $request->input($af_val) . '%');
                    }
                }
            }

            if (Common::requestHas($request, 'name')) {
                $query->whereRaw('CONCAT(first_name, \' \' , last_name)  like \'%' . $request->input('name') . '%\'');
            }

            $queryCount = $query->count();

            if (Common::requestHas($request, 'offset')) {
                $query->offset($request->input('offset'));
            }

            if (Common::requestHas($request, 'limit')) {
                $query->limit($request->input('limit'));
            }

            if (Common::requestHas($request, 'sorting_key') && Common::requestHas($request, 'sorting_seq')) {
                $query->orderBy($request->input('sorting_key'), $request->input('sorting_seq'));
            }

            $query = $query->get();


            if ($query->isEmpty()) {
                return \API::jsonResponse(['result' => \APIResultResponse::SUCCESS, 'error' => $this->table_desc . ' is not found'], \APIHttpResponse::OK);
            }

            foreach ($query as $queryData) {
                $roles = $this->convertListOfObjectsToArrays(ModelCustom::findOrFail($queryData['id'])->roles->makeHidden('pivot'), 'name');
                $queryData['roles'] = $roles;
            }

            $data['data'] = $query;
            $data['count'] = $queryCount;
            return \API::jsonResponse(['result' => \APIResultResponse::SUCCESS, 'data' => $data, 'message' => $this->table_desc . ' has been searched successfully'], \APIHttpResponse::OK);
        } catch (\Exception $e) {
            return \API::jsonResponse(['result' => \APIResultResponse::ERROR, 'error' => $e->getMessage()], \APIHttpResponse::ERROR);
        }
    }

    public function dropdown(Request $request)
    {
        try {
            // validate rules
            $rules = [
                'status'             =>   'required'
            ];

            $all_rules = array_merge([], $rules);
            $validator = Validator::make($request->all(), $all_rules);

            // if failed, return result
            if ($validator->fails()) {
                return \API::jsonResponse(['result' => \APIResultResponse::ERROR, 'error' => $validator->errors()], \APIHttpResponse::VALIDATION_ERROR);
            }

            $validatedPostData = $validator->validated();

            $data = [];
            $customers = DB::table('customers')
                ->whereNull('deleted_at')
                ->where('status', '=', $request->input('status'))
                ->select(DB::raw('CAST(max(id) AS char(255)) as value, CONCAT(first_name, " ", last_name) as label'))
                ->groupBy('id');

            $customers = $customers->get();

            if ($customers->isEmpty()) {
                return \API::jsonResponse(['result' => \APIResultResponse::NOT_EXIST, 'error' => 'Customer is not found'], \APIHttpResponse::OK);
            }

            $data['data'] = $customers;
            $data['count'] = $customers->count();

            return \API::jsonResponse(['result' => \APIResultResponse::SUCCESS, 'data' => $data, 'message' => 'Customer has been retreived successfully'], \APIHttpResponse::OK);
        } catch (\Exception $e) {
            return \API::jsonResponse(['result' => \APIResultResponse::ERROR, 'error' => $e->getMessage()], \APIHttpResponse::ERROR);
        }
    }


    private function validateCustom(Request $request, $action = 'create', $id = 0)
    {

        if ($action == 'create') {
            $default_rules = [
                'email' => [
                    'required',
                    Rule::unique('customers')->whereNull('deleted_at')
                ],
                'password' => 'required'
            ];
        } else if ($action == 'update') {
            $default_rules = [
                'email' => [
                    'required',
                    Rule::unique('customers')->ignore($id)->whereNull('deleted_at')
                ]
            ];
        }

        return Validator::make($request->all(), $default_rules);
    }

    private function validateRoles(Request $request)
    {
        $default_rules = [
            'roles' => "required|array|min:1",
            'roles.*' => "required",
        ];

        return Validator::make($request->all(), $default_rules);
    }

    private function convertListOfObjectsToArrays($objects, $column)
    {
        $data = array();

        foreach ($objects as $object) {
            array_push($data, $object->{$column});
        }

        return $data;
    }
}
