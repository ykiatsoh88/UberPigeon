<?php

namespace App\Lib\Auth;

use App\Models\Customer;
use Validator;
use Carbon\Carbon;
use ErrorMessage;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use DB;
use App\Events\CustomerRegistered;
use App\Lib\OTP\OTP;
use App\Events\ResendVerificationEmail;
use App\Models\ConsolidatedUser;

class CustomerRegistration
{

    private const MANUAL = 0;

    use ValidatesRequests;

    public function create(Request $request)
    {
        try{
            return $this->createNewAccount($request);
        } catch (\Exception $e) {
            return \API::jsonResponse(['result'=> \APIResultResponse::ERROR,'error' =>  ErrorMessage::show($e)  ],\APIHttpResponse::ERROR);
        }
        
    }

    private function createNewAccount(Request $request){

        $validator = $this->validateRequest($request);

        // return fail result
        if ($validator->fails()) {
            return \API::jsonResponse(['result'=> \APIResultResponse::VALIDATION_ERROR ,'error' => $validator->errors() ],\APIHttpResponse::VALIDATION_ERROR);
        }

        // assign valid data to data variable in array format
        $data = $validator->valid(); 

        $data['status'] = 1;

        // register customer to database
        $customer = Customer::create($data);

        // send email verification
        event(new CustomerRegistered($customer));

        return \API::jsonResponse(['result'=> \APIResultResponse::SUCCESS ,"message" => "Successful creation of account , please verify account via your email" ],\APIHttpResponse::OK);

    }

    private function validateRequest(Request $request)
    {
     
        $default_rules = [
            'first_name'    => 'required|max:150',
            'last_name'     => 'required|max:150',
            // 'country_id'    => 'required|numeric',
            'email' => [
                'required',
                Rule::unique('customers')->whereNull('deleted_at')
            ],
			'phone_no'      => 'required|unique:customers',
			'password'      => [ 'required','min:6' ]
        ];

        return Validator::make($request->all(), $default_rules);
    }

    public function resendVerificationEmail(Request $request)
    {

        try{
            
            $default_rules = [
                'email'      => 'required|email',
            ];
            
            // validate email
            if (($validator = Validator::make($request->all(), $default_rules))->fails())
                return \API::jsonResponse(['result'=> \APIResultResponse::VALIDATION_ERROR ,'error' => $validator->errors() ],\APIHttpResponse::VALIDATION_ERROR);

            $data = $validator->valid();

            // if customer already verified, skip it
            if( Customer::where('email',$data['email'])->where('is_email_verified', 1)->exists())
                return \API::jsonResponse(['result'=> \APIResultResponse::FAIL ,'error' => 'Customer already verified' ],\APIHttpResponse::BAD_REQUEST);

            $customer = Customer::where('email', $data['email'])->first();

            //TO-DO : Check the verificaiton count if > 10 times then dont allow #not-done-yk
             
            // event log for resend verification
            event(new ResendVerificationEmail($customer));
            
            // update customer retries count
            $customer->email_verify_retries += 1;
            $customer->save();
             
             return \API::jsonResponse(['result'=> \APIResultResponse::SUCCESS ,'message' => "Successfully Sent" ],\APIHttpResponse::OK);
             
        } catch (\Exception $e) {
            return \API::jsonResponse(['result'=> \APIResultResponse::ERROR ,'error' => ErrorMessage::show($e) ],\APIHttpResponse::ERROR);
        }
        
    }

    public function verifyAccount(Request $request)
    {

        try{

            $default_rules = [
                'code'       => 'required',
                'email'      => 'required|email',
                'otp'        => 'required',
            ];
           
            if (($validator = Validator::make($request->all(), $default_rules))->fails())
                return \API::jsonResponse(['result'=> \APIResultResponse::VALIDATION_ERROR,'error' =>   $validator->errors() ],\APIHttpResponse::VALIDATION_ERROR);

            $data = $validator->valid();
          
            if(!$validOTP = (new OTP())->VerifyOTP($data['email'],$data['otp'],$data['code']))
                return \API::jsonResponse(['result'=> \APIResultResponse::OTP_INVALID,'error' =>  'Invalid OTP' ],\APIHttpResponse::BAD_REQUEST);

            if( Customer::where('email',$data['email'])->where('is_email_verified', 1)->exists())
                return \API::jsonResponse(['result'=> \APIResultResponse::FAIL ,'error' =>  'Customer already verified' ],\APIHttpResponse::BAD_REQUEST);

            Customer::where('email',$data['email'])->update(['status'=> 1 , 'is_email_verified' => 1 , 'email_verified_at' => now()]);
             
             return \API::jsonResponse(['result'=> \APIResultResponse::SUCCESS ,'message' => "Customer Successfully verified" ],\APIHttpResponse::OK);

        } catch (\Exception $e) {
            return \API::jsonResponse(['result'=> \APIResultResponse::ERROR ,'error' => ErrorMessage::show($e) ],\APIHttpResponse::ERROR);
        }
        
    }

}