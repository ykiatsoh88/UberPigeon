<?php
namespace App\Lib\Auth;

use App\Exceptions\CustomerDisabledException;
use App\Exceptions\CustomerNotFoundException;
use App\Models\Customer;
use Carbon\Carbon;
use ErrorMessage;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use JWTAuth;
use Str;
use TheSeer\Tokenizer\Exception;
use Validator;
use Tymon\JWTAuth\Exceptions\TokenExpiredException;
use Tymon\JWTAuth\Exceptions\TokenInvalidException;
use Tymon\JWTAuth\Exceptions\JWTException;

/**
 * This custom class for new accout registrations
 */
class CustomerAuth
{

    private const MANUAL_LOGIN = 0;
    private const SOCIAL_MEDIA_LOGIN = 1;

    private Customer $dBCustomer;

    use ValidatesRequests;

    public function login(Request $request, $login_method = SELF::MANUAL_LOGIN)
    {

        try {

            $validator = $this->validateRequest($request, $login_method);

            if ($validator->fails()) {
               return \API::jsonResponse(['result'=> \APIResultResponse::VALIDATION_ERROR,'error' => $validator->errors()  ],\APIHttpResponse::VALIDATION_ERROR);
            }

            // assign valid data to data variable in array format
            $data = $validator->valid(); 

            // Check if exist then assign to dBCustomer
            $this->verifyCustomerExistInDB($data['login_credential'], $login_method); 

            // checks if the password if valid or not
            $isPasswordValid = $this->checkPasswordValidation($data['password']); 

            if (!$isPasswordValid) {
                return \API::jsonResponse(['result'=> \APIResultResponse::FAIL,'error' => 'Invalid Password'  ],\APIHttpResponse::UNAUTHORIZED);
            }

            // update customer token
            $newlyGeneratedJWTtoken = $this->generateJWTToken();

            // update last login time
            $this->updateCustomerDetails(); 

            return \API::jsonResponse(['result'=> \APIResultResponse::SUCCESS, 'data'=> $this->getNewToken($newlyGeneratedJWTtoken, $this->dBCustomer)  ],\APIHttpResponse::OK);

        } catch (CustomerDisabledException $e) {
           return \API::jsonResponse(['result'=> \APIResultResponse::DISABLED,'error' => "Customer Disabled"  ],\APIHttpResponse::FORBIDDEN);
        } catch (CustomerNotFoundException $e) {
           return \API::jsonResponse(['result'=> \APIResultResponse::NOT_EXIST ,'error' => "Customer Not Exist"  ],\APIHttpResponse::NOT_FOUND);
        } catch (\Exception $e) {
           return \API::jsonResponse(['result'=> \APIResultResponse::ERROR ,'error' => ErrorMessage::show($e)  ],\APIHttpResponse::ERROR);
        }

    }

    private function validateRequest(Request $request, $login_method = SELF::MANUAL_LOGIN)
    {
        $default_rules = [];

        $rules = [
            'login_credential' => 'required|max:255',
            'password' => 'required',
        ];

        $all_rules = array_merge($default_rules, $rules);

        return Validator::make($request->all(), $all_rules, $this->customValidationMsg());
    }

    private function verifyCustomerExistInDB($login_credential, $login_method = SELF::MANUAL_LOGIN)
    {
        $customerExist = false;

        // check if it is a phone number and remove the +
        if (is_numeric($login_credential)) {

            $login_credential = preg_replace('/^0\+/', '', $login_credential); // removing the + in front
            $calling_code = Str::substr($login_credential, 0, 2); // removing the calling code 2 for now as China and Msia is +60 and +86
            $login_credential = Str::substr($login_credential, 2, strlen($login_credential) - 2);
        }

        switch ($login_method) {
            case SELF::MANUAL_LOGIN:
                $customer = Customer::where('email', $login_credential)
                    ->orWhere('phone_no', $login_credential)
                    ->first();
                break;
        }

        if (empty($customer)) {
            throw new CustomerNotFoundException();
        }

        // Check disabled status of Customer and if empty throw exceptions
        if (!empty($customer) && $customer->status == 0) {
            throw new CustomerDisabledException();
        }

        $this->dBCustomer = $customer;

        return ($customerExist = true);
    }

    private function checkPasswordValidation($keyInPassword)
    {
        return \Hash::check($keyInPassword, $this->dBCustomer->password) ? true : false;
    }

    // not yet completed full function #not-done-yk
    private function generateJWTToken()
    {

        // manual generate token from Customer model
        $customer = $this->dBCustomer; // token = JWTSubject which customers module extended
        $token = auth('user-api')->login($customer);
        //
        // $token_expire            = Carbon::createFromTimestamp($unix_time)->diffInSeconds();

        // \Redis::rpush("customer_token:" . $customer->id, $token);
        // \Redis::hset('all_customer_token', $token, $customer->id);

        return $token;

    }

    private function updateCustomerDetails()
    {
        $this->dBCustomer->last_login = Carbon::now();
        $this->dBCustomer->save();
    }

    private function customValidationMsg()
    {

        return [
            'login_credential.required' => 'Your login credential Mobile No/Email is required ',
            'email.required' => 'Your Email is required '
        ];

    }

    public function logout(Request $request )
    {

        try {

            $token = $request->header( 'Authorization' );
            JWTAuth::parseToken()->invalidate( $token );

            return \API::jsonResponse(['result'=> \APIResultResponse::SUCCESS, 'message' => "Logout Success"  ],\APIHttpResponse::OK);

        } catch (TokenExpiredException $e) {
            return \API::jsonResponse(['result'=> \APIResultResponse::TOKEN_EXPIRED,'error' => trans('auth.token.expired')  ],\APIHttpResponse::UNAUTHORIZED);
        } catch (TokenInvalidException $e) {
            return \API::jsonResponse(['result'=> \APIResultResponse::TOKEN_INVALID,'error' => trans('auth.token.invalid')  ],\APIHttpResponse::UNAUTHORIZED);
        } catch (JWTException $e) {
            return \API::jsonResponse(['result'=> \APIResultResponse::TOKEN_NOT_PROVIDED,'error' =>trans('auth.token.not.provided')  ],\APIHttpResponse::UNAUTHORIZED);
        }catch (Exception $e) {
            return \API::jsonResponse(['result'=> \APIResultResponse::ERROR,'error' => trans('auth.token.expired')  ],\APIHttpResponse::ERROR);
        }

    }

    /**
     * Refreshes the token based on a valid expired JWT token
     */
    public function refreshToken(Request $request)
    {

        try{

            \Config::set('auth.providers.users.model', \App\Models\Customer::class);
            $auth = JWTAuth::parseToken();
           
            // Get token
            if (!$old_token = $auth->setRequest($request)->getToken()) {
                return \API::jsonResponse(['result'=> \APIResultResponse::TOKEN_INVALID, 'error' => 'Not Authorized to access'  ],\APIHttpResponse::UNAUTHORIZED);
            }

            // Try to refresh the Expired Token and reassign new token
            $newlyGeneratedJWTtoken = JWTAuth::refresh($old_token);
            $customer = auth('user-api')->setToken($newlyGeneratedJWTtoken)->user();

            return \API::jsonResponse(['result'=> \APIResultResponse::SUCCESS, 'message' => "Refresh Success" , 'data' =>$this->getNewToken($newlyGeneratedJWTtoken, $customer) ],\APIHttpResponse::OK);

        } catch (TokenExpiredException $e) {
            return \API::jsonResponse(['result'=> \APIResultResponse::TOKEN_EXPIRED,'error' => trans('auth.token.expired')  ],\APIHttpResponse::UNAUTHORIZED);
        } catch (TokenInvalidException $e) {
            return \API::jsonResponse(['result'=> \APIResultResponse::TOKEN_INVALID,'error' => trans('auth.token.invalid')  ],\APIHttpResponse::UNAUTHORIZED);
        } catch (JWTException $e) {
            return \API::jsonResponse(['result'=> \APIResultResponse::TOKEN_NOT_PROVIDED,'error' =>trans('auth.token.not.provided')  ],\APIHttpResponse::UNAUTHORIZED);
        }catch(TokenBlacklistedException $e){
            return \API::jsonResponse(['result'=> \APIResultResponse::TOKEN_BLACKLISTED,'error' =>trans('auth.token.blacklisted')  ],\APIHttpResponse::UNAUTHORIZED);
        }catch (Exception $e) {
            return \API::jsonResponse(['result'=> \APIResultResponse::ERROR,'error' => trans('auth.token.expired')  ],\APIHttpResponse::ERROR);
        }

    }

    private function getNewToken($token, $customer)
    {
        return [
            'result'       => config('response.success'),
            'access_token' => $token,
            'token_type' => 'bearer',
            'expires_in' => auth('user-api')->factory()->getTTL(),
            'customer' => $customer->only(['name', 'email', 'id', 'is_email_verified']),
        ];
    }

}
