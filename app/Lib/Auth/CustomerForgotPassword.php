<?php

namespace App\Lib\Auth;

use DB;
use Validator;
use ErrorMessage;
use Carbon\Carbon;
use App\Lib\OTP\OTP;
use App\Models\Customer;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use App\Events\ForgotPassword;
use Illuminate\Support\Facades\Password;
use Illuminate\Foundation\Validation\ValidatesRequests;

class CustomerForgotPassword
{

    use ValidatesRequests;

    public function forgotPassword(Request $request)
    {
        try
        {
            $default_rules = [
                'email'      => 'required|email',
            ];

            if (($validator = Validator::make($request->all(), $default_rules))->fails())
                return \API::jsonResponse(['result'=> \APIResultResponse::VALIDATION_ERROR ,'error' => $validator->errors() ],\APIHttpResponse::VALIDATION_ERROR);

            $data = $validator->valid();

            if( !(Customer::where('email',$data['email'])->where('is_email_verified', 1)->exists()))
                return \API::jsonResponse(['result'=> \APIResultResponse::FAIL ,'error' => 'Customer with the email address is not exists or valid' ],\APIHttpResponse::BAD_REQUEST);

            $customer = Customer::where('email',$data['email'])->first();

            event(new ForgotPassword($customer));

            return \API::jsonResponse(['result'=> \APIResultResponse::SUCCESS ,'message' => "Reset password link has been sent to your email." ],\APIHttpResponse::OK);
        }
        catch (\Exception $e) {
            return \API::jsonResponse(['result'=> \APIResultResponse::ERROR ,'error' => ErrorMessage::show($e) ],\APIHttpResponse::ERROR);
        }
    }
}
