<?php

namespace App\Lib\Auth;

use DB;
use Validator;
use ErrorMessage;
use Carbon\Carbon;
use App\Lib\OTP\OTP;
use App\Models\Customer;
use Illuminate\Http\Request;
use App\Events\ForgotPassword;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Password;
use Illuminate\Foundation\Validation\ValidatesRequests;

class CustomerResetPassword
{

    use ValidatesRequests;

    public function verifyResetPassword(Request $request)
    {
        try{

            $default_rules = [
                'code'       => 'required',
                'email'      => 'required|email',
                'otp'        => 'required',
            ];

            if (($validator = Validator::make($request->all(), $default_rules))->fails())
                return \API::jsonResponse(['result'=> \APIResultResponse::VALIDATION_ERROR,'error' =>   $validator->errors() ],\APIHttpResponse::VALIDATION_ERROR);

            $data = $validator->valid();

            if(!$validOTP = (new OTP())->VerifyOTP($data['email'],$data['otp'],$data['code']))
                return \API::jsonResponse(['result'=> \APIResultResponse::OTP_INVALID,'error' =>  'Invalid OTP' ],\APIHttpResponse::BAD_REQUEST);

            if(!$validReset = (Cache::has($data['otp'])))
                return \API::jsonResponse(['result'=> \APIResultResponse::FAIL ,'error' =>  'Reset Password is invalid or expired.' ],\APIHttpResponse::BAD_REQUEST);

             return \API::jsonResponse(['result'=> \APIResultResponse::SUCCESS ,'message' => "Reset Password is valid." ],\APIHttpResponse::OK);

        } catch (\Exception $e) {
            return \API::jsonResponse(['result'=> \APIResultResponse::ERROR ,'error' => ErrorMessage::show($e) ],\APIHttpResponse::ERROR);

        }
    }

    public function resetPassword(Request $request)
    {

        try {
            
            $default_rules = [
                'password' => 'required|min:6',
                'otp' => 'required',
                'email' => 'required|email'
            ];

            if (($validator = Validator::make($request->all(), $default_rules))->fails())
                return \API::jsonResponse(['result'=> \APIResultResponse::VALIDATION_ERROR,'error' =>   $validator->errors() ],\APIHttpResponse::VALIDATION_ERROR);

            $data = $validator->valid();

            if(!$validReset = (Cache::has($data['otp'])))
                return \API::jsonResponse(['result'=> \APIResultResponse::FAIL ,'error' =>  'Reset Password is invalid or expired.' ],\APIHttpResponse::BAD_REQUEST);

            Customer::where('email',$data['email'])->update(['password'=> bcrypt($data['password'])]);

            // Remove cache
            Cache::forget($data['otp']);

            return \API::jsonResponse(['result'=> \APIResultResponse::SUCCESS ,'message' => "Password has been reset successfully." ],\APIHttpResponse::OK);

        } catch (\Exception $e) {
            return \API::jsonResponse(['result'=> \APIResultResponse::ERROR ,'error' => ErrorMessage::show($e) ],\APIHttpResponse::ERROR);
        }
    }
}
