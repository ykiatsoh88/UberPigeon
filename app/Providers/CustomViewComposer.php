<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use SideMenu;

class CustomViewComposer extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {

        view()->composer('layouts.menu', function ($view) {

            // if ($this->app->environment() != 'production') {
            //     $env = ucfirst($this->app->environment());
            //     // $env = ( env('JAVASHOP_ENV' , 0 ) == 1 ? 'A5B_'.$env: $env );
            // }
            $sideMenuItems = SideMenu::getAllMenus();

            $view->with('sideMenuItems', $sideMenuItems);

        });

        // view()->composer('components.indicator' , function($view){
        //     $env  = "";

        //     if($this->app->environment()  != 'production'){
        //         $env = ucfirst($this->app->environment()) ;
        //        // $env = ( env('JAVASHOP_ENV' , 0 ) == 1 ? 'A5B_'.$env: $env );
        //     }

        //     $view->with('environment' , $env );

        // });

        // view()->composer('vendor.log-viewer' , function($view){
        //     $env  = "";

        //     if($this->app->environment()  != 'production'){
        //         $env = ucfirst($this->app->environment()) ;
        //     }

        //     $view->with('environment' , $env );

        // });
        // // $language_list = collect(['English']);

        // if (!app()->runningInConsole()) {
        //     $language_list = Language::orderBy('sort_order')->get();
        // }

        //  $data['languages']       = $language_list;
        //  $data['language_apply'] = "en";

        //  \View::share('config',$data);

    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
