<?php

namespace App\Providers;

// use Illuminate\Auth\Events\Registered;
// use Illuminate\Auth\Listeners\SendEmailVerificationNotification;
use App\Listeners\ClearCart;
use App\Events\ForgotPassword;
use App\Events\CustomerRegistered;
use App\Events\SalesOrderPaidAction;
use App\Listeners\WelcomeNewCustomer;
use App\Listeners\AddJobToBeTrigger;
use App\Listeners\NotifyViaCallbackURL;
use Illuminate\Support\Facades\Event;
use App\Events\ResendVerificationEmail;
use App\Listeners\SendOrderSuccessfulEmail;
use App\Events\WebhookAction;
use App\Events\NewCustomerHasRegisteredEvent;
use App\Listeners\SendEmailVerificationNotification;
use App\Listeners\SendEmailResetPasswordNotification;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;
use Spatie\WebhookClient\Models\WebhookCall;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        CustomerRegistered::class =>[
            SendEmailVerificationNotification::class
        ],
        WebhookAction::class=>[
            AddJobToBeTrigger::class,
            NotifyViaCallbackURL::class,
        ],
        ResendVerificationEmail::class => [
            SendEmailVerificationNotification::class
        ],
        ForgotPassword::class => [
            SendEmailResetPasswordNotification::class
        ],
        SalesOrderPaidAction::class => [
            ClearCart::class,
            SendOrderSuccessfulEmail::class
        ],
    ];


    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();

        //
    }
}

