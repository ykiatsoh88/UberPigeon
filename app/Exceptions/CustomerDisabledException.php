<?php

namespace App\Exceptions;

use Exception;

class CustomerDisabledException extends Exception
{
    protected $message = 'Customer Disabled';

}
