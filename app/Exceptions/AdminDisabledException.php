<?php

namespace App\Exceptions;

use Exception;

class AdminDisabledException extends Exception
{
    protected $message = 'Admin Disabled';

}
