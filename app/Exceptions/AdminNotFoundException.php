<?php

namespace App\Exceptions;

use Exception;

class AdminNotFoundException extends Exception
{
    protected $message = 'Admin Not Found';

}
