<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;


class FileUploadController extends Controller
{
    public function showUploadForm()
    {
        return view('temp');
    }


    public function storeUploads(Request $request)
    {
       $img_url = \Imager::upload($request->file('file')->getRealPath() , "test");

       if($request->ajax())
            return $img_url;
        
        $success = "File uploaded";

        return view('temp')
            ->with(compact('img_url','success'));
    
    }

}