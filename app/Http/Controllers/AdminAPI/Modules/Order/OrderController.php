<?php

namespace App\Http\Controllers\AdminAPI\Modules\Order;

use Validator;
use ErrorMessage;

use App\Http\Controllers\Controller;
use App\Models\Order;
use App\Models\Pigeon;

use Illuminate\Support\Facades\DB;
use Illuminate\Validation\Rule;
use Illuminate\Http\Request;
use Illuminate\Foundation\Validation\ValidatesRequests;

class OrderController extends Controller
{

    use ValidatesRequests;

    private $status = ['pending'=>0, 'accepted'=>1, 'rejected'=>2, 'cancelled'=>3, 'received'=>4];

    // admin reject order
    // accepted order only allow to reject
    public function rejectOrder(Request $request)
    {

        DB::beginTransaction();
        
        try {

            // validate rules
            $rules = [
                'pigeon_id'  => 'required|numeric|exists:App\Models\Customer,id,deleted_at,NULL',
                'order_id'   => 'required|numeric|exists:App\Models\Order,id,deleted_at,NULL,pigeon_id,'.$request->input('pigeon_id'),
                'reason'     => 'nullable|max:255'
            ];

            $validator = Validator::make($request->input(), $rules);
            
            // failed validate
            if ($validator->fails()) {
                return \API::jsonResponse(['result'=> \APIResultResponse::ERROR, 'error' => $validator->errors()], \APIHttpResponse::ERROR);
            }


            $post_data = $validator->validated();

            $order = Order::findOrFail($post_data['order_id']);

            // only pending status allow to change to reject status
            if( $order->status != $this->status['pending'] ){
                $message = 'Invalid order status! Reject order only applicable order which status in pending';

                return \API::jsonResponse(['result'=> \APIResultResponse::ERROR, 'error' => $message], \APIHttpResponse::ERROR);
            }


            // update order status to reject
            if(isset($post_data['reason'])){ 
                $order->reject_reason  = $post_data['reason']; 
            }
            
            $order->status         = $this->status['rejected']; // 2 = rejected
            $order->save();
            

            // success response
            DB::commit();

            $message = 'Order has been successfully rejected';

            return \API::jsonResponse(['result'=> \APIResultResponse::SUCCESS, 'data'=> [], 'message' => $message], \APIHttpResponse::OK);

        } catch (\Exception $e) {

            // failed & rollback 
            DB::rollBack();

            return \API::jsonResponse(['result'=> \APIResultResponse::ERROR, 'error' => ErrorMessage::show($e)], \APIHttpResponse::ERROR);

        }

    }

}
