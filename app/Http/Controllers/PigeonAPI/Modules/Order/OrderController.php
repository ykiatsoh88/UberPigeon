<?php

namespace App\Http\Controllers\PigeonAPI\Modules\Order;

use Validator;
use ErrorMessage;

use App\Http\Controllers\Controller;
use App\Models\Order;
use App\Models\Pigeon;
use App\Models\PigeonOrder;

use Illuminate\Support\Facades\DB;
use Illuminate\Validation\Rule;
use Illuminate\Http\Request;
use Illuminate\Foundation\Validation\ValidatesRequests;

class OrderController extends Controller
{

    use ValidatesRequests;

    // temporary hard code for status
    private $status = ['pending'=>0, 'accepted'=>1, 'rejected'=>2, 'cancelled'=>3, 'received'=>4];

    // pigeon accept order
    // pending order only allow to accept
    public function acceptOrder(Request $request)
    {

        DB::beginTransaction();

        try {

            // validate rules
            $rules = [
                'pigeon_id'  => 'required|numeric|exists:App\Models\Customer,id,deleted_at,NULL',
                'order_id'   => 'required|numeric|exists:App\Models\Order,id,deleted_at,NULL'
            ];

            $validator = Validator::make($request->input(), $rules);
            
            // failed validate
            if ($validator->fails()) {
                return \API::jsonResponse(['result'=> \APIResultResponse::ERROR, 'error' => $validator->errors()], \APIHttpResponse::ERROR);
            }

            $post_data = $validator->validated();


            $order  = Order::findOrFail($post_data['order_id']);
            $pigeon = Pigeon::findOrFail($post_data['pigeon_id']);

            // only pending status allow to change to accept status
            if( $order->status != $this->status['pending'] ){
                $message = 'Invalid order status! Accpet order only applicable order which status in pending';

                return \API::jsonResponse(['result'=> \APIResultResponse::ERROR, 'error' => $message], \APIHttpResponse::ERROR);
            }


            // validate pigeon have active order or not 
            $pigeon_order = PigeonOrder::where('status', 1)->where('pigeon_id', $post_data['pigeon_id'])->get();

            // failed because this pigeon is still have active order to deliver
            if( $pigeon_order->count() > 0 ){
                $message = 'The pigeons still have order have not been delivered';

                return \API::jsonResponse(['result'=> \APIResultResponse::ERROR, 'error' => $message], \APIHttpResponse::ERROR);
            }

            // get estimate delivery time
            // distance / speed = hours 
            $delivery_time = $order->distance / $pigeon->speed;
            $time_array = explode('.', $delivery_time);

            $add_time = (int)($time_array[0] * 60) + (($delivery_time - $time_array[0]) * 60);
            $add_time = (int)$add_time;
            
            $estimate_delivery_time = date('Y-m-d H:i:s', strtotime("+{$add_time} minutes"));


            // update order status to reject
            $order->pigeon_id   = $post_data['pigeon_id']; // assign pigeon to order
            $order->status      = $this->status['accepted']; // 1 = accepted
            $order->save();

            // add active order to pigeon
            $pigeon_order = new PigeonOrder;
            $pigeon_order->pigeon_id                = $post_data['pigeon_id']; 
            $pigeon_order->order_id                 = $post_data['order_id']; 
            $pigeon_order->estimate_delivery_time   = $estimate_delivery_time; 
            $pigeon_order->status                   = 1; // 1 = active
            $pigeon_order->save();
            
            // success response
            DB::commit();

            $message = 'Order has been successfully accepted';

            return \API::jsonResponse(['result'=> \APIResultResponse::SUCCESS, 'data'=> [], 'message' => $message], \APIHttpResponse::OK);

        } catch (\Exception $e) {

            // failed & rollback 
            DB::rollBack();

            return \API::jsonResponse(['result'=> \APIResultResponse::ERROR, 'error' => ErrorMessage::show($e)], \APIHttpResponse::ERROR);

        }

    }

    // pigeon reject order
    // accepted order only allow to reject
    public function rejectOrder(Request $request)
    {

        DB::beginTransaction();
        
        try {

            // validate rules
            $rules = [
                'pigeon_id'  => 'required|numeric|exists:App\Models\Customer,id,deleted_at,NULL',
                'order_id'   => 'required|numeric|exists:App\Models\Order,id,deleted_at,NULL,pigeon_id,'.$request->input('pigeon_id'),
                'reason'     => 'nullable|max:255'
            ];

            $validator = Validator::make($request->input(), $rules);
            
            // failed validate
            if ($validator->fails()) {
                return \API::jsonResponse(['result'=> \APIResultResponse::ERROR, 'error' => $validator->errors()], \APIHttpResponse::ERROR);
            }


            $post_data = $validator->validated();

            $order = Order::findOrFail($post_data['order_id']);

            // only accepted status allow to change to reject status
            if( $order->status != $this->status['accepted'] ){
                $message = 'Invalid order status! Reject order only applicable order which status in accepted';

                return \API::jsonResponse(['result'=> \APIResultResponse::ERROR, 'error' => $message], \APIHttpResponse::ERROR);
            }


            // update order status to reject
            if(isset($post_data['reason'])){ 
                $order->reject_reason  = $post_data['reason']; 
            }
            
            $order->status         = $this->status['rejected']; // 2 = rejected
            $order->save();
            

            // update pigeon order to inactive
            $pigeon_order = PigeonOrder::where('status', 1)->where('pigeon_id', $post_data['pigeon_id'])->where('order_id', $post_data['order_id'])->first();

            $pigeon_order->status  = 0; // 0 = inactive
            $pigeon_order->save();


            // @todo penalty for pigeon 

            // success response
            DB::commit();

            $message = 'Order has been successfully rejected';

            return \API::jsonResponse(['result'=> \APIResultResponse::SUCCESS, 'data'=> [], 'message' => $message], \APIHttpResponse::OK);

        } catch (\Exception $e) {

            // failed & rollback 
            DB::rollBack();

            return \API::jsonResponse(['result'=> \APIResultResponse::ERROR, 'error' => ErrorMessage::show($e)], \APIHttpResponse::ERROR);

        }

    }

    // pigeon mark order as receive
    // accepted order only allow to receive
    public function orderReceive(Request $request)
    {

        DB::beginTransaction();

        try {

            // validate rules
            $rules = [
                'pigeon_id'  => 'required|numeric|exists:App\Models\Customer,id,deleted_at,NULL',
                'order_id'   => 'required|numeric|exists:App\Models\Order,id,deleted_at,NULL,pigeon_id,'.$request->input('pigeon_id')
            ];

            $validator = Validator::make($request->input(), $rules);
            
            // failed validate
            if ($validator->fails()) {
                return \API::jsonResponse(['result'=> \APIResultResponse::ERROR, 'error' => $validator->errors()], \APIHttpResponse::ERROR);
            }


            $post_data = $validator->validated();

            $order          = Order::findOrFail($post_data['order_id']);
            $pigeon         = Pigeon::findOrFail($post_data['pigeon_id']);

            // only accepted status allow to change to receive status
            if( $order->status != $this->status['accepted'] ){
                $message = 'Invalid order status! Order receive only applicable order which status in accepted';

                return \API::jsonResponse(['result'=> \APIResultResponse::ERROR, 'error' => $message], \APIHttpResponse::ERROR);
            }


            // calculate costing for invoice purpose
            $total_cost = round($order->distance * $pigeon->cost);

            // update order status to receive
            $order->status = $this->status['received']; // 4 = received
            $order->total  = $total_cost; 
            $order->save();

            // update pigeon order to inactive
            $pigeon_order = PigeonOrder::where('status', 1)->where('pigeon_id', $post_data['pigeon_id'])->where('order_id', $post_data['order_id'])->first();

            $pigeon_order->status               = 0; // 0 = inactive
            $pigeon_order->actual_delivery_time = date('Y-m-d H:i:s'); // now
            $pigeon_order->save();

            // success response
            DB::commit();

            $message = 'Order has been note as status received';

            return \API::jsonResponse(['result'=> \APIResultResponse::SUCCESS, 'data'=> [], 'message' => $message], \APIHttpResponse::OK);

        } catch (\Exception $e) {

            // failed & rollback 
            DB::rollBack();

            return \API::jsonResponse(['result'=> \APIResultResponse::ERROR, 'error' => ErrorMessage::show($e)], \APIHttpResponse::ERROR);

        }

    }

}
