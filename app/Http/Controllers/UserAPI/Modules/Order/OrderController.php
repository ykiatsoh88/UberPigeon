<?php

namespace App\Http\Controllers\UserAPI\Modules\Order;

use Validator;
use ErrorMessage;

use App\Http\Controllers\Controller;
use App\Models\Order;

use Illuminate\Support\Facades\DB;
use Illuminate\Validation\Rule;
use Illuminate\Http\Request;
use Illuminate\Foundation\Validation\ValidatesRequests;

class OrderController extends Controller
{

    use ValidatesRequests;

    private $status = ['pending'=>0, 'accepted'=>1, 'rejected'=>2, 'cancelled'=>3, 'received'=>4];

    // customer submit order
    public function submitOrder(Request $request)
    {

        DB::beginTransaction();

        try {

            // validate rules
            $rules = [
                'customer_id'           => 'required|numeric|exists:App\Models\Customer,id,deleted_at,NULL',
                'deadline'              => 'required|date',
                'distance'              => 'required|numeric|min:1',
                'pickup_address'        => 'required',
                'pickup_coordinates'    => 'required|regex:/^([-+]?\d{1,2}([.]\d+)?),\s*([-+]?\d{1,3}([.]\d+)?)$/',
                'delivery_address'      => 'required',
                'delivery_coordinates'  => 'required|regex:/^([-+]?\d{1,2}([.]\d+)?),\s*([-+]?\d{1,3}([.]\d+)?)$/',
                'remark'                => 'nullable|max:255',
            ];

            $validator = Validator::make($request->input(), $rules);
            
            // failed validate
            if ($validator->fails()) {
                return \API::jsonResponse(['result'=> \APIResultResponse::ERROR, 'error' => $validator->errors()], \APIHttpResponse::ERROR);
            }

            $post_data = $validator->validated();

            // validate valid deadline
            $deadline = strtotime($post_data['deadline']);
            $now = time();

            if( $now >= $deadline ){
                $message = 'Invalid deadline! Deadline must greater than now';

                return \API::jsonResponse(['result'=> \APIResultResponse::ERROR, 'error' => $message], \APIHttpResponse::ERROR);
            }


            // @todo weight, document image, running number for order


            // insert order
            $order = new Order;

            if(isset($post_data['remark'])){
                $order->remark                  = $post_data['remark'];
            }

            $order->customer_id             = $post_data['customer_id'];
            $order->deadline                = $post_data['deadline'];
            $order->distance                = $post_data['distance'];
            $order->pickup_address          = $post_data['pickup_address'];
            $order->pickup_coordinates      = $post_data['pickup_coordinates'];
            $order->delivery_address        = $post_data['delivery_address'];
            $order->delivery_coordinates    = $post_data['delivery_coordinates'];
            $order->pigeon_id               = 0;
            $order->status                  = 0; // 0 = pending status
            $order->save();
            
            $order_id = $order->id;


            // @todo weight, image, running number for order


            // success response
            DB::commit();

            $message = 'Order has been successfully placed';

            return \API::jsonResponse(['result'=> \APIResultResponse::SUCCESS, 'data'=> $order, 'message' => $message], \APIHttpResponse::OK);

        } catch (\Exception $e) {

            // failed & rollback 
            DB::rollBack();

            return \API::jsonResponse(['result'=> \APIResultResponse::ERROR, 'error' => ErrorMessage::show($e)], \APIHttpResponse::ERROR);

        }

    }

    // customer cancel order
    // customer able to cancel before any pigeon accept this order
    // pending order only allow to cancel
    public function cancelOrder(Request $request)
    {

        DB::beginTransaction();

        try {

            // validate rules
            $rules = [
                'cust_id'    => 'required|numeric|exists:App\Models\Customer,id,deleted_at,NULL',
                'order_id'   => 'required|numeric|exists:App\Models\Order,id,deleted_at,NULL,customer_id,'.$request->input('cust_id'),
                'reason'     => 'nullable|max:255'
            ];

            $validator = Validator::make($request->input(), $rules);
            
            // failed validate
            if ($validator->fails()) {
                return \API::jsonResponse(['result'=> \APIResultResponse::ERROR, 'error' => $validator->errors()], \APIHttpResponse::ERROR);
            }

            $post_data = $validator->validated();

            $order = Order::findOrFail($post_data['order_id']);

            // only pending status allow to change to reject status
            if( $order->status != $this->status['pending'] ){
                $message = 'Invalid order status! Cancel order only applicable order which status in pending';

                return \API::jsonResponse(['result'=> \APIResultResponse::ERROR, 'error' => $message], \APIHttpResponse::ERROR);
            }

            // update order status to reject
            $order->cancel_reason  = $post_data['reason'];
            $order->status         = $this->status['cancelled']; // 3 = cancel
            $order->save();
            
            // success response
            DB::commit();

            $message = 'Order has been successfully cancelled';

            return \API::jsonResponse(['result'=> \APIResultResponse::SUCCESS, 'data'=> [], 'message' => $message], \APIHttpResponse::OK);

        } catch (\Exception $e) {

            // failed & rollback 
            DB::rollBack();

            return \API::jsonResponse(['result'=> \APIResultResponse::ERROR, 'error' => ErrorMessage::show($e)], \APIHttpResponse::ERROR);

        }

    }

    // @todo get customer order history list
    public function getOrderHistory(Request $request){
    }

    // @todo get available pigeon list
    public function getAvailablePigeons(Request $request){
    }

    // @todo get delivery estimate price
    // estimate by using average cost
    public function getEstimatePrice(Request $request){
    }

    // @todo view customer order invoice
    public function viewInvoice(Request $request){
    }

    // @todo download customer order invoice in pdf
    // using dompdf may be
    public function downloadInvoice(Request $request){
    }

    // @todo get order detail info
    public function getOrder(Request $request){
    }

}
