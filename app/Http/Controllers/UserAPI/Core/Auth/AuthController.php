<?php

namespace App\Http\Controllers\UserAPI\Core\Auth;

use JWTAuth;
use Response;

use Carbon\Carbon;
use App\Models\Customer;
use App\Lib\Auth\CustomerAuth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;

use App\Lib\Auth\CustomerRegistration;
use Illuminate\Support\Facades\Auth;
use App\Lib\Auth\CustomerForgotPassword;
use App\Lib\Auth\CustomerResetPassword;
use Illuminate\Support\Facades\Validator;
use Tymon\JWTAuth\Exceptions\TokenBlacklistedException;

class AuthController extends Controller
{

    public function __construct() { }

    public function login(Request $request){
        return (new CustomerAuth())->login($request);
    }

    public function logout( Request $request ) {

        return (new CustomerAuth())->logout($request);
    }

    public function register(Request $request){
        return (new CustomerRegistration())->create($request);
    }

    public function refresh(Request $request) {
        return (new CustomerAuth())->refreshToken($request);
    }

    public function resendVerificationEmail(Request $request){
        return (new CustomerRegistration())->resendVerificationEmail($request);
    }
    
    public function verifyAccount(Request $request){
        return (new CustomerRegistration())->verifyAccount($request);
    }

    public function forgotPassword(Request $request) {
        return (new CustomerForgotPassword())->forgotPassword($request);
    }

    public function verifyResetPassword(Request $request) {
        return (new CustomerResetPassword())->verifyResetPassword($request);
    }

    public function resetPassword(Request $request)
    {
        return (new CustomerResetPassword())->resetPassword($request);
    }

}
