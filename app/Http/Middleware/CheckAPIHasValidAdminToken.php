<?php

namespace App\Http\Middleware;

use Auth;
use Closure;
use JWTAuth;
use Symfony\Component\HttpKernel\Exception\UnauthorizedHttpException;
use Tymon\JWTAuth\Exceptions\JWTException;
use Tymon\JWTAuth\Exceptions\TokenExpiredException;
use Tymon\JWTAuth\Exceptions\TokenInvalidException;
use Exception;

class CheckAPIHasValidAdminToken
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */

    public function handle($request, Closure $next)
    {

        try {

            \Config::set('jwt.user', \App\Models\User::class);
            \Config::set('auth.providers.users.model', \App\Models\User::class);

            $auth    = JWTAuth::parseToken();
            $payload = $auth->getPayload();
            $token   = $auth->getToken();


            // if  token is valid and not adm (Admin guard only )which is Admin only api , unauthorized
            if ($payload->get('grd') !== "adm") {
                return response()->json(['error' => true,'message' => trans('auth.invalid.token'),], 401);
            }

            // if  token is valid and is adm but the subject is unauthorized
            if (!$admin = auth('admin-api')->setToken($token)->authenticate()) {
                return response()->json([ 'error' => true, 'message' => trans('auth.invalid.admin.user'),], 401);
            }

            return $next($request);

        } catch (TokenExpiredException $exception) {
            return response()->json(['result' => config('response.token_expired') , 'message' => trans('auth.token.expired'), ], 401);
        } catch (TokenInvalidException $exception) {
            return response()->json(['result' => config('response.token_invalid'),'message' => trans('auth.token.invalid'), ], 401);
        } catch (JWTException $exception) {
            return response()->json(['result' => config('response.token_not_provided'),'message' => trans('auth.token.not.provided'), ], 500);
        }catch (Exception $e) {
            return response()->json(['result' => config('response.error'),'message' => ErrorMessage::show($e), ], 500);
        }

        

    }

}
