<?php

namespace App\Http\Middleware;

use Closure;
use Config;
use App;
use Session;

class Language
{
   
    public function handle($request, Closure $next)
    {
       
        if(\Session::has('locale')){
            $locale = \Session::get('locale', Config('settings.default-localisation'));
        }else{
            $locale = Config('settings.default-localisation');
        }

        App::setLocale($locale);

       
        $data['apply'] = $locale;
        $data['language_selected'] = ($locale === "en" ? "ENGLISH" : "中文");
        
        /**
         * This variable is available globally on all your views, and sub-views
         */
        view()->share('global_lang',  $data);


        return $next($request);
    }

}
