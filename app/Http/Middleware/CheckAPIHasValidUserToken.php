<?php

namespace App\Http\Middleware;

use Auth;
use Closure;
use JWTAuth;
use Symfony\Component\HttpKernel\Exception\UnauthorizedHttpException;
use Tymon\JWTAuth\Exceptions\JWTException;
use Tymon\JWTAuth\Exceptions\TokenExpiredException;
use Tymon\JWTAuth\Exceptions\TokenInvalidException;
use Exception;

class CheckAPIHasValidUserToken
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */

    public function handle($request, Closure $next)
    {

        try {

            \Config::set('jwt.user', \App\Models\Customer::class);
            \Config::set('auth.providers.users.model', \App\Models\Customer::class);

            $auth    = JWTAuth::parseToken();
            $payload = $auth->getPayload();
            $token   = $auth->getToken();


            // if  token is valid and not usr (User guard only )which is User only api , unauthorized
            if ($payload->get('grd') !== "usr") {
                return response()->json(['result' => config('response.invalid_api_access'),'message' => trans('auth.invalid.token'),], 401);
            }

            // if  token is valid and is usr but the subject is unauthorized
            if (!$user = auth('user-api')->setToken($token)->authenticate()) {
                return response()->json(['result' => config('response.invalid_api_access') , 'message' => trans('auth.invalid.user'),], 401);
            }

            // Set User Id from Auth as input customer id
            $request->merge(['customer_id' => auth('user-api')->user()->id]);

            return $next($request);

        } catch (TokenExpiredException $exception) {
            return response()->json(['result' => config('response.token_expired') , 'message' => trans('auth.token.expired'), ], 401);
        } catch (TokenInvalidException $exception) {
            return response()->json(['result' => config('response.token_invalid'),'message' => trans('auth.token.invalid'), ], 401);
        } catch (JWTException $exception) {
            return response()->json(['result' => config('response.token_not_provided'),'message' => trans('auth.token.not.provided'), ], 500);
        }catch (Exception $e) {
            return response()->json(['result' => config('response.error'),'message' => ErrorMessage::show($e), ], 500);
        }



    }

}
