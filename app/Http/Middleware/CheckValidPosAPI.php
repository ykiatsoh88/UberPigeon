<?php

namespace App\Http\Middleware;

use Auth;
use Closure;
use Exception;
use App\Helpers\Common;
use App\Models\Customer;

class CheckValidPosAPI
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */

    public function handle($request, Closure $next)
    {

        try {

            $token   = env('POS_API_KEY');

            $bearerToken = $request->bearerToken();

            // Check Valid token
            if ($bearerToken != $token) {
                return response()->json(['result' => config('response.token_invalid') , 'message' => 'Invalid POS Token'], 401);
            }

            return $next($request);
        } catch (Exception $e) {
            return response()->json(['result' => config('response.error'), 'message' => ErrorMessage::show($e),], 500);
        }
    }
}
