<?php

namespace App\Listeners;

use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use App\Events\CustomerRegistered;
use Illuminate\Support\Facades\Mail;
use App\Mail\newCustomerEmailVerification;

class SendEmailVerificationNotification implements ShouldQueue
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
    }

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle($event)
    {
        $email = $event->customer->email;
        $locale = "en";
        Mail::to($email)->send((new newCustomerEmailVerification($event->customer))->locale($locale));
    }
}
