<?php

namespace App\Listeners;

use App\Events\WebhookAction;
use App\Mail\WelcomeNewUserMail;
use App\Models\CustomersWebHook;
use Illuminate\Support\Facades\Mail;
use Spatie\WebhookServer\WebhookCall;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class NotifyViaCallbackURL
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function handle($event)
    {
        $items = $event->data;
        $customer_id = $items['customer_id'];
        $data = CustomersWebHook::where('customer_id','=',$customer_id)->first();

        WebhookCall::create()
        ->url($data['SERVER_URL'])
        ->payload(['data' => $event->data])
        ->useSecret($data['SECRET_KEY'])
        ->dispatch();
        
    }
}
