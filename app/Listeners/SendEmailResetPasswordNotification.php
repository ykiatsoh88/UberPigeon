<?php

namespace App\Listeners;

use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use App\Events\ForgotPassword;
use Illuminate\Support\Facades\Mail;
use App\Mail\newResetPasswordEmail;

class SendEmailResetPasswordNotification implements ShouldQueue
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
    }

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle($event)
    {
        $email = $event->customer->email;
        $locale = "en";
        Mail::to($email)->send((new newResetPasswordEmail($event->customer))->locale($locale));
    }
}
