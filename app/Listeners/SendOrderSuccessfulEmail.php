<?php

namespace App\Listeners;

use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use App\Events\SalesOrderPaidAction;
use Illuminate\Support\Facades\Mail;
use App\Mail\orderSuccessfulEmail;
use App\Models\Member;

class SendOrderSuccessfulEmail implements ShouldQueue
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle($event) {

        // $email = $event->member->email;
        // $locale = "en";
        // Mail::to($email)->send((new orderSuccessfulEmail($event->member))->locale($locale));
        
        $member_id = $event->salesOrder->member_id;
        $member = Member::findOrFail($member_id);
        $email = $member->email;
        $locale = "en";
        Mail::to($email)->send((new orderSuccessfulEmail($member))->locale($locale));
    }
}
