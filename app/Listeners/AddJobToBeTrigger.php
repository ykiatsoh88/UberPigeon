<?php

namespace App\Listeners;

use App\Events\WebhookAction;
use Spatie\WebhookServer\WebhookCall;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class AddJobToBeTrigger
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  WebhookAction  $event
     * @return void
     */
    public function handle(WebhookAction $event)
    {
        
    }
}
