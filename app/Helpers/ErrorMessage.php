<?php

namespace App\Helpers;

use Arr;
use Exceptions;

class ErrorMessage
{

    public static function show($ex , $customErrorMessage = null, $displayEvenProductionEnv = false)
    {


        if (config('app.env') !== 'production' ){

            if (filled($customErrorMessage))
                return  $customErrorMessage . " - " . $ex->getMessage();

            return $ex->getMessage();
        }
        
        if ($displayEvenProductionEnv ){

            if (filled($customErrorMessage))
                return  $customErrorMessage . " - " . $ex->getMessage();

            return $ex->getMessage();
        }
           
           
        if (filled($customErrorMessage))
            return  $customErrorMessage;


        return __('error.msg.notification');
    }

}