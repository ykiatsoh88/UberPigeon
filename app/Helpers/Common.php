<?php

namespace App\Helpers;

use File;
use Arr;
use Carbon\Carbon;
use Illuminate\Http\Request;

class Common
{

    public static function adminPrefix()
    {
        return env('ADMIN_PREFIX', 'admin');
    }

    public static function getFilesRealPath(string $directory, iterable $excludedFiles = null)
    {

        if (empty($directory) || is_null($directory)) {
            return null;
        }

        $fileList = [];
        $files = File::files($directory);

        foreach ($files as $file) {
            $fileRealPath = $file->getRealPath();
            $fileName = $file->getFilename();
            $fileList += [$fileName => $fileRealPath];
        }

        if (!is_null($excludedFiles) && $excludedFiles->isNotEmpty()) {

            foreach ($excludedFiles as $files) {
                $fileList = Arr::Except($fileList, [$files]);
            }
        }

        return $fileList;

    }

    public static function isEmptyNull($variable, $check_zero = false)
    {

        if ($check_zero) {
            if (is_null($variable) || (empty($variable) && $variable !== '0')) {
                return true;
            }

        } else {
            if (is_null($variable) || empty($variable)) {
                return true;
            }

        }

        return false;

    }

    public static function array_depth(array $array)
    {
        $max_depth = 1;

        foreach ($array as $value) {
            if (is_array($value)) {
                $depth = Self::array_depth($value) + 1;

                if ($depth > $max_depth) {
                    $max_depth = $depth;
                }
            }
        }

        return $max_depth;
    }

    public static function requestHas(Request &$request, $queryItem, $checkForZeroString = false)
    {
        return ($request->has($queryItem) && !self::isEmptyNull($request->input($queryItem), $checkForZeroString)) ? true : false;

    }

    public static function getLocale()
    {
        return \Lang::locale();
    }

    public static function getLocaleId()
    {
        return \Lang::locale() == "en" ? 1 : 2;
    }

    public static function getCurrentDateTime(){
        return Carbon::now()->toDateTimeString();
    }

}
