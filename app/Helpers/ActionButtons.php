<?php

namespace App\Helpers;

use Arr;

class ActionButtons
{

    public static function generate(array $btns_list, $data = null, $module_details = null)
    {

        $btnHTML = "";

        if (count($btns_list) <= 0) {
            return $btnHTML;
        }

        $module_id = 0;

        // Get Module ID from the module details
        if (!Common::isEmptyNull($module_details)) {
            $module_id = $module_details->get('id');
        }

        // Convert Single Array to Nested Array
        if (Common::array_depth($btns_list) <= 1) {
            $btns_list = [$btns_list];
        }

        //$types = collect($types_settings)->keyBy('type');
        $btns_list = collect($btns_list);

        $btns_list->each(function ($btnConfig, $index) use (&$btnHTML) {

            $actionBtn = new ActionButton();
            self::setActionButtonData($actionBtn, $btnConfig);

            $btn_str = "<a title='%s' class='btn btn-%s %s-btn %s %s' style='margin-top:2px'  href='%s' data-id='%s' data='%s' data-toggle='%s' data-target='%s'><i class='fa %s'></i>%s</a> ";

            $btnHTML .= vsprintf($btn_str, [$actionBtn->title, $actionBtn->size, $actionBtn->name, $actionBtn->color, $actionBtn->addCssClass, $actionBtn->href, $actionBtn->dataId, $actionBtn->data, $actionBtn->dataToggle, $actionBtn->dataTarget, $actionBtn->icon, $actionBtn->text]);

        });

        return $btnHTML;
    }

    private static function setActionButtonData(ActionButton $actionBtn, $actionBtnConfig)
    {

        // Base Config
        $btnConfigs = [
            "default" => ["buttonColor" => "btn-info", "button_icon" => "fa-search", "button_name" => "view", "permission_key" => "v"],
            "view" => ["buttonColor" => "btn-info", "button_icon" => "fa-search", "button_name" => "view", "permission_key" => "v"],
            "edit" => ["buttonColor" => "btn-success", "button_icon" => "fa-edit", "button_name" => "edit", "permission_key" => "e"],
            "edit2" => ["buttonColor" => "btn-success", "button_icon" => "fa-edit", "button_name" => "edit2", "permission_key" => "e2"],
            "create" => ["buttonColor" => "btn-primary", "button_icon" => "fa-plus", "button_name" => "create", "permission_key" => "c"],
            "approve" => ["buttonColor" => "btn-warning", "button_icon" => "fa-approve", "button_name" => "approve", "permission_key" => "a"],
            "delete" => ["buttonColor" => "btn-danger", "button_icon" => "fa-trash", "button_name" => "delete", "permission_key" => "d"],
            "detail" => ["buttonColor" => "btn-warning", "button_icon" => "fa-info-circle", "button_name" => "detail", "permission_key" => "dt"],
            "custom" => ["buttonColor" => "btn-danger", "button_icon" => "fa-info-question", "button_name" => "custom", "permission_key" => "cc"],
            "custom2" => ["buttonColor" => "btn-danger", "button_icon" => "fa-info-question", "button_name" => "custom2", "permission_key" => "cc2"],
            "custom3" => ["buttonColor" => "btn-danger", "button_icon" => "fa-info-question", "button_name" => "custom3", "permission_key" => "cc3"],
            "refresh" => ["buttonColor" => "btn-primary", "button_icon" => "fa-sync", "button_name" => "refresh", "permission_key" => "b"],
            "clone" => ["buttonColor" => "btn-primary", "button_icon" => "fa-clone", "button_name" => "clone", "permission_key" => "cl" ],
        ];

        $btnType = strtolower(Arr::get($actionBtnConfig, 'type')); // get Btn type view , edit , delete , custom , detail
        $selectedBtnConfig = collect($btnConfigs)->get($btnType); // Get Base Config based on button Type

        // Check if Btn Config has Overriding properties , will override the base props if exist
        $actionBtn->color = Arr::get($actionBtnConfig, 'color', arr::get($selectedBtnConfig, "buttonColor"));
        $actionBtn->icon = Arr::get($actionBtnConfig, 'icon', arr::get($selectedBtnConfig, "button_icon"));
        $actionBtn->name = Arr::get($actionBtnConfig, 'name', arr::get($selectedBtnConfig, "button_name"));
        $actionBtn->permission_key = Arr::get($actionBtnConfig, 'permission-key', arr::get($selectedBtnConfig, "permission_key"));
        $actionBtn->size = Arr::get($actionBtnConfig, 'size', "sm"); // bootstrap size : xs , sm , md , lg
        $actionBtn->title = Arr::get($actionBtnConfig, 'title', $btnType); // title for button anchor < a title="?">
        $actionBtn->data = Arr::get($actionBtnConfig, 'data', "");
        $actionBtn->dataId = Arr::get($actionBtnConfig, 'data-id', "");
        $actionBtn->addCssClass = Arr::get($actionBtnConfig, 'class', "");
        $actionBtn->text = Arr::get($actionBtnConfig, 'text', "");
        $actionBtn->href = Arr::get($actionBtnConfig, 'href', Arr::get($actionBtnConfig, 'href', '#'));
        $actionBtn->dataToggle = Arr::get($actionBtnConfig, 'dataToggle', "");
        $actionBtn->dataTarget = Arr::get($actionBtnConfig, 'dataTarget', "");


        // // to capture onclick js attached to btn
        // if (filled($on_click_str)) {
        //     $on_click_str = 'onClick="' . $on_click_str . '"';
        // }

        return $actionBtn;
    }

}

class ActionButton
{
    public $href;
    public $color;
    public $Icon;
    public $name;
    public $permissionKey;
    public $text;
    public $size;
    public $title;
    public $addCssClass; // adding custom class so that javascript can pickup
    public $data; // member name , etc
    public $dataTarget;
    public $dataToggle;
    public $dataId; // member_id , user_id , etc

}
