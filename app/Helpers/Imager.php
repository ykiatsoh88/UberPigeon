<?php

namespace App\Helpers;

use Arr;
use Exceptions;

class Imager
{

  public static function upload($imageFile , $folder = null){

    return cloudinary()->upload($imageFile ,  ['folder' => $folder ])->getSecurePath();

  }

// Only supports Cloudinary Images as it uses their transformation api
  public static function thumbnailIt($url , $height=100 , $width=150){

    $pos =  strpos($url , 'upload/') + 7;

    $replacement ="c_limit,h_$height,w_$width/";
    $thumbnail_url = substr_replace($url,  $replacement, $pos , 0);

    return $thumbnail_url ;

  }

  

}