<?php

namespace App\Helpers;

class SideMenu
{

    public static function getAllMenus()
    {
        $sidebarList = collect();

        // // excluded file in module folder
        // $excluded_files = collect(['permission.php'])->flip();

        foreach (glob(base_path('app/Http/Controllers/AdminPortal/Modules/*/Sidebar.php')) as $filename) {
            $sidebarFile = include $filename;

            if (!empty($sidebarFile) && is_array($sidebarFile)) {
                $sidebarFile = collect($sidebarFile);

                if ($sidebarFile->has('data')) {
                    $sidebarList->push($sidebarFile);
                }

            }

        }

        if ($sidebarList->count() > 0) {
            $sidebarList = $sidebarList->sortBy(function ($item, $key) {
                return collect($item->first())->get('order_number');
            });

            //      $sidebarItem = [];
            // $sidebarItem = array_merge($sidebarItem, $item->get('sidebar_items'));

            $sidebarItem = collect();

            $sidebarList->each(function ($item, $key) use (&$sidebarItem) {

                $sidebarItem->push($item->get('sidebar_items'));

            });

        }

        return $sidebarList;

    }

    public static function setActive($route, $routesActive)
    {
        // collect($item->get('submenu'))->pluck('route')
        if (is_object($routesActive)) {
            $routesActive = collect($routesActive)->pluck('route');

            return in_array($route, $routesActive) ? 'active' : '';
        }

        return $route->getName() == $routesActive ? 'active' : '';
        //    if (is_array($route)) {
        //     return in_array(Request::path(), $route) ? 'active' : '';
        // }
        // return Request::path() == $route ? 'active' : '';
    }

}
