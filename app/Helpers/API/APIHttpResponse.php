<?php

class APIHttpResponse {

    const OK    = 200;
    const BAD_REQUEST = 400;
    const UNAUTHORIZED = 401;
    const FORBIDDEN = 403;
    const NOT_FOUND = 404;
    const CONFLICT = 409;
    const VALIDATION_ERROR = 422; // Unprocessable Entity
    const ERROR = 500;

    public static function getStatusByID($resultResponseID){
        $reflectionClass = new ReflectionClass(static::class);
        $responses = collect($reflectionClass->getConstants());
        return $responses->flip()->get($resultResponseID);
    }

}

