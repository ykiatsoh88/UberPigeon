<?php

class APIResultResponse {

    const SUCCESS               = 1;
    const FAIL                  = 10;
    const NOT_EXIST             = 11;
    const DISABLED              = 12;
    const VALIDATION_ERROR      = 20;
    const INVALID_REQUEST_FORMAT = 21;
    const INVALID_API_ACCESS    = 100;
    const TOKEN_INVALID         = 300;
    const TOKEN_EXPIRED         = 310;
    const TOKEN_BLACKLISTED     = 333;
    const TOKEN_NOT_PROVIDED    = 355;
    const OTP_INVALID           = 400;
    const ERROR                 = 500;
    const SQL_ERROR             = 560;
    const MAINTENANCE           = 888;

    public static function getStatusByID($resultResponseID){
        $reflectionClass = new ReflectionClass(static::class);
        $responses = collect($reflectionClass->getConstants());
        return $responses->flip()->get($resultResponseID);
    }

}
