<?php


class API
{


    public static function jsonResponse(  $data ,  $httpCode) {
        
      if(! Arr::exists($data, 'result'))
            throw new Exception('Missing Result Parameter');

        $error    = (Arr::exists($data, "error") ?   data_get($data,'error') : null) ;
        $result  =  (Arr::exists($data, 'result') ?  data_get( $data, 'result' ) : null );
        $message =  (Arr::exists($data, 'message') ? data_get( $data, 'message' ) : null) ;
        $tmp_data    =  (Arr::exists($data, 'data') ?   data_get( $data, 'data' ) : null );


       $content =  ['result' => $result, 
                    'message' => $message ,
                    'error' =>  $error ,
                    'data'  => $tmp_data
                    ];


        return response()->json( $content, $httpCode);
    }

    public static function arrayResponse(  $data ,  $httpCode) {
        
        if(! Arr::exists($data, 'result'))
              throw new Exception('Missing Result Parameter');
  
          $error       = (Arr::exists($data, "error") ?   data_get($data,'error') : null) ;
          $result      =  (Arr::exists($data, 'result') ?  data_get( $data, 'result' ) : null );
          $message     =  (Arr::exists($data, 'message') ? data_get( $data, 'message' ) : null) ;
          $tmp_data    =  (Arr::exists($data, 'data') ?   data_get( $data, 'data' ) : null );
  
  
         $content =  [
                      'result' => $result, 
                      'message' => $message ,
                      'error' =>  $error ,
                      'data'  => $tmp_data,
                      'httpCode' => $httpCode
                      ];
  
  
          return $content ;
      }

}




