const mix = require("laravel-mix");

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js("resources/js/app.js", "public/js")
    .sass("resources/sass/app.scss", "public/css")
    .sourceMaps()
    .js("node_modules/popper.js/dist/popper.js", "public/js");
   


// .sass("node_modules/toastr/toastr.scss", "public/css")
mix.copy(["node_modules/admin-lte/plugins/daterangepicker/daterangepicker.css",
            "resources/css/override-app.css",
            "node_modules/admin-lte/plugins/jquery-ui/jquery-ui.min.css",
            "node_modules/admin-lte/plugins/summernote/summernote-bs4.min.css",
            "node_modules/admin-lte/plugins/tempusdominus-bootstrap-4/css/tempusdominus-bootstrap-4.min.css",
            "node_modules/admin-lte/plugins/ekko-lightbox/ekko-lightbox.css",
], "public/css");



mix.scripts(
    [
        "node_modules/admin-lte/plugins/jquery/jquery.min.js",
        "node_modules/admin-lte/plugins/jquery-ui/jquery-ui.min.js",
        "node_modules/admin-lte/plugins/bootstrap/js/bootstrap.bundle.min.js",
        "node_modules/admin-lte/plugins/datatables/jquery.dataTables.min.js",
        "node_modules/admin-lte/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js",
        "node_modules/admin-lte/plugins/datatables-responsive/js/dataTables.responsive.min.js",
        "node_modules/admin-lte/plugins/datatables-responsive/js/responsive.bootstrap4.min.js",
        "node_modules/admin-lte/plugins/select2/js/select2.full.min.js",
        "node_modules/admin-lte/plugins/toastr/toastr.min.js",
        "node_modules/admin-lte/plugins/moment/moment.min.js",
        "node_modules/admin-lte/plugins/daterangepicker/daterangepicker.js",
        "node_modules/admin-lte/plugins/inputmask/jquery.inputmask.min.js",
        "node_modules/admin-lte/plugins/inputmask/bindings/inputmask.binding.js",
        "node_modules/admin-lte/plugins/summernote/summernote-bs4.min.js",
        "node_modules/jquery-confirm/dist/jquery-confirm.min.js",
        "node_modules/admin-lte/plugins/ekko-lightbox/ekko-lightbox.min.js",
        "node_modules/admin-lte/dist/js/adminlte.min.js",
        "resources/js/custom.js"
    ],
    "public/js/all.min.js"
).copy(["node_modules/toastr/build/toastr.js.map",
"node_modules/admin-lte/plugins/moment/moment.min.js.map",
"node_modules/admin-lte/plugins/summernote/summernote-bs4.min.js.map",
"node_modules/admin-lte/dist/js/adminlte.min.js.map",
"node_modules/admin-lte/plugins/ekko-lightbox/ekko-lightbox.min.js.map",
], "public/js");


