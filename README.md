# Project UberPigeon

## How to initiate the project (first time only)

1. Open Git bash at any desired folder
1. Git clone to your local machine (bash), open project in vscode
    ```bash 
    git clone https://gitlab.com/ykiatsoh88/UberPigeon.git 
    ``` 
1. At VSCode, press `CTRL+SHIFT+P`, type `terminal:selectDefaultShell`, use arrow key to select `Git Bash`, press enter
1. press `CTRL+SHIFT+P` again, type `terminal: Create New Integrated Terminal`, use arrow to select it, press enter
1. in VSCode Integrated Bash terminal, run command `sh init.sh` to install all the required dependencies, until `initialize complete...` appears
    ```bash 
    sh init.sh
    ```
1. by default, the site will be available at  - `localhost:10033`
1. "php artisan migate --seed" to install table to database

## How to access MySQL database
1. by default for adminer  - `localhost:10034`
1. by default for phpmyadmin - `localhost:10035`

## How to use docker for this project
1. How to down the project
    ```bash 
    docker-compose down 
    ``` 
1. How to up the project again
    ```
    docker-compose up -d 
    ```

## How to use composer or php artisan command for this project
1. Run command `sh shell.sh` to access php docker container
1. once access you may see terminal change into `www-data@xxx:/application$` then u would know you are at the container
1. apply any `php artisan` command or `composer` command at this container environment

<br>
<br>


## Site can be view If traefik is initialized
1. open host file editor add new line
    ~~~
    127.0.0.1 api.uberpigeon.test
    127.0.0.1 adminer.uberpigeon.test
    127.0.0.1 phpmyadmin.uberpigeon.test
    ~~~
1. by default the site will also be available at both 
    - `localhost:10033` or `api.uberpigeon.test`
1. by default for adminer will also be available at both
    - `localhost:10034` or `adminer.uberpigeon.test`
1. by default for phpmyadmin will also be available at both
    - `localhost:10035` or `phpmyadmin.uberpigeon.test`

<br>
<br>

## UberPigeon simple order flow
1. Customer submit order, enter delivery address & pickup address, deadline
    ~~~
    - Customer able to cancel order before any pigeon accept order
    - Admin able to reject order before any pigeon accept order
    ~~~
1. Available pigeon earn commission by accept order 
    ~~~
    - pigeon still able to reject order after accepted order, but will have penalty on them
    ~~~
1. After delivery to customer, pigeon will open app to change order status to received
    ~~~
    - system will calculate order total amount & generate invoice to customer
    ~~~
