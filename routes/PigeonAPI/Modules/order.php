<?php

// pigeon
Route::post('reject-order', 'Modules\Order\OrderController@rejectOrder')->name('pigeons.api.reject.order');
Route::post('accept-order', 'Modules\Order\OrderController@acceptOrder')->name('pigeons.api.accept.order');
Route::post('order-receive', 'Modules\Order\OrderController@orderReceive')->name('pigeons.api.order.receipt');