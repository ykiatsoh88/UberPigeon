<?php

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
 */

// User API v1
Route::group(['prefix' => 'users'], function () {

    Route::group(['prefix' => 'v1', 'namespace' => 'UserAPI'], function () {

        require base_path('routes/UserAPI/Core/auth.php');

        Route::group(['middleware' => ['api.auth.user']], function () {

            $excluded_routes_file = Collect([]); // e.g collect(['products.php']);
            $module_routes_files = Common::getFilesRealPath(base_path('routes/UserAPI/Modules'), $excluded_routes_file);
            $core_routes_files = Common::getFilesRealPath(base_path('routes/UserAPI/Core'), Collect(['auth.php']));

            $all_route_files = array_merge($module_routes_files, $core_routes_files);

            foreach ($all_route_files as $file => $file_real_path) {
                require $file_real_path;
            }
        });
    });
});

// Admin API v1
Route::group(['prefix' => 'admins'], function () {
    // Admin API v1
    Route::group(['prefix' => 'v1', 'namespace' => 'AdminAPI'], function () {

        //require base_path('routes/AdminAPI/Core/auth.php');

        //Route::group(['middleware' => ['api.auth.admin']], function () {

            $excluded_routes_file = Collect([]); // e.g collect(['products.php']);
            $module_routes_files = Common::getFilesRealPath(base_path('routes/AdminAPI/Modules'), $excluded_routes_file);

            $all_route_files = $module_routes_files;

            foreach ($all_route_files as $file => $file_real_path) {
                require $file_real_path;
            }
        //});
    });
});

// Pigeon API v1
Route::group(['prefix' => 'pigeons'], function () {
    // Pigeon API v1
    Route::group(['prefix' => 'v1', 'namespace' => 'PigeonAPI'], function () {

        //require base_path('routes/PigeonAPI/Core/auth.php');

        //Route::group(['middleware' => ['api.auth.pigeon']], function () {

            $excluded_routes_file = Collect([]); // e.g collect(['products.php']);
            $module_routes_files = Common::getFilesRealPath(base_path('routes/PigeonAPI/Modules'), $excluded_routes_file);

            $all_route_files = $module_routes_files;

            foreach ($all_route_files as $file => $file_real_path) {
                require $file_real_path;
            }
        //});
    });
});