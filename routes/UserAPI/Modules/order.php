<?php

// customer 
Route::post('submit-order', 'Modules\Order\OrderController@submitOrder')->name('users.api.submit.order');
Route::post('cancel-order', 'Modules\Order\OrderController@cancelOrder')->name('users.api.cancel.order');