<?php

Route::post('auth.register', 'Core\Auth\AuthController@register')->name('users.api.auth.register');
Route::post('auth.login', 'Core\Auth\AuthController@login')->name('users.api.auth.login');
Route::post('auth.refresh.token', 'Core\Auth\AuthController@refresh')->name('users.api.auth.refresh.token');
Route::post('auth.resend.verification.email', 'Core\Auth\AuthController@resendVerificationEmail')->name('users.api.auth.resend.verification.email');
Route::post('auth.verify.email', 'Core\Auth\AuthController@verifyAccount')->name('users.api.auth.verify.account');
Route::post('auth.forgot.password', 'Core\Auth\AuthController@forgotPassword')->name('users.api.auth.forgot.password');
Route::post('auth.verify.reset.password', 'Core\Auth\AuthController@verifyResetPassword')->name('users.api.auth.verify.reset.password');
Route::post('auth.reset.password', 'Core\Auth\AuthController@resetPassword')->name('users.api.auth.reset.password');

Route::group(['middleware'=> ['api.auth.user']], function () {
    Route::post('auth.logout', 'Core\Auth\AuthController@logout')->name('users.api.auth.logout');
});